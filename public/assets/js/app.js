(function() {
  $(document).ready(function() {
    $('select').material_select();
    $('.modal-trigger').leanModal();
    $('#wrapper-elementos').hide();
    $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 300,
      constrain_width: true,
      hover: false,
      gutter: 0,
      belowOrigin: true,
      alignment: 'left'
    });
    $(".button-collapse").sideNav();
    //====================================================================================
    //====================================================================================
    var resta = $(document).innerHeight() - $('#app-section-main').innerHeight();
    if (resta > 300) {
      $('footer').addClass('fixed');
    } else {
      $('footer').addClass('relative');
    }
    //====================================================================================
    //====================================================================================
    $(".search-dina").keyup(function() {
        //var paramProveedor = $(this).data('proveedor');
        var input = $(this).val() != '' ? $(this).val() : 'none';
        var nombre = $(this).data('nombre');
        var idSelect = $(this).data('id-select');
        $.get("busqueda-proveedores-" + nombre + "/" + input, function(response, state) {
          $("#" + idSelect).empty();
          if (input == 'none') {
            $("#" + idSelect).append(" <option value='' disabled selected>Escoge el o los proveedores</option>")
          }
          for (var i = 0; i < response.length; i++) {
            $("#" + idSelect).append("<option value='" + response[i].id + "'>" + response[i].empresa + "</option>");
          };
          $("#" + idSelect).material_select();
        })
      })
      //Select Dinámico
      //=================================================
    function getValueEleCatRef(event) {
      $.get("codigo-elemento-referencia/" + event.target.value, function(response, state) {
        $('#consecutivo').val(response);
      })
    }
    $('#categoria-id').change(function(event) {
        getValueEleCatRef(event);
      })
      //====================================================================================
      //====================================================================================
    $(".search").keyup(function() {
      var idSelect = $(this).data('id-select');
      var idInput = $(this).attr('id');
      searchSel(idInput, idSelect);
    });

    function searchSel(idInput, idSelect) {
      var input = $("#" + idInput).val();
      var output = document.getElementById(idSelect).options;
      for (var i = 0; i < output.length; i++) {

        if ($("#" + idSelect + " option[value='" + output[i].value + "']").text().indexOf(input) == 0) {
          output[i].selected = true;
          $("#" + idSelect).material_select();
          if (idInput === 'serch-cat') {
            $.get("codigo-elemento-referencia/" + output[i].value, function(response, state) {
              $('#consecutivo').val(response);
            })
          }
        }
        if (document.getElementById(idInput).value == '') {
          output[0].selected = true;
        }
      }

    }
    //====================================================================================
    //====================================================================================
  });
}).call(this);
//====================================================================================
//====================================================================================
(function() {
  $(document).ready(function() {
    $('.ver-proveedores').on('click', function() {
      var paramProveedor = $(this).data('proveedor');
      var paramVerMaterialID = $(this).data('ver');
      var paramTabla = $(this).data('tabla');
      var paramTablaRel = $(this).data('tabla-rel');
      var paramColId = $(this).data('id-param');
      var paramVerMaterialNombre = $(this).data('nombre');
      $.get("proveedor-" + paramProveedor + "/" + paramTabla + "/" + paramTablaRel + "/" + paramColId + "/" + paramVerMaterialID, function(response, state) {
        $('#result-materiales').empty();
        console.log(response);
        for (var i = 0; i < response.length; i++) {
          $('#result-materiales').append('<tr class="center-align"><td class="center-align">' + response[i].id + '</td><td  class="center-align">' + response[i].empresa + '</td><td  class="center-align">' + response[i].direccion + '</td><td  class="center-align">' + response[i].telefono + '</td> <td  class="center-align">' + response[i].celular + '</td><td class="center-align">' + response[i].email + '</td></tr>');
        };
      });
      $('#titulo-material').html("Material" + " " + paramVerMaterialNombre);
      $('#modal1').openModal();
    })
    $('.ver-insupros').on('click', function() {
      var paramVerMaterialID = $(this).data('ver');
      console.log(paramVerMaterialID);
      $.get("referencia-insupros/insupros/" + paramVerMaterialID, function(response, state) {
        $('#result-materiales').empty();
        for (var i = 0; i < response.length; i++) {
          $('#result-materiales').append('<tr class="center-align"><td class="center-align">' + response[i].id + '</td><td  class="center-align">' + response[i].nombre + '</td><td  class="center-align">' + '<a href="insupros/' + response[i].id + '/edit" type="button" class="btn"> Editar</button>' + '</td></tr>');
        };
      });
      $('#titulo-material').html("Insupros");
      $('#modal2').openModal();
    })
  })
}).call(this);
//====================================================================================
//====================================================================================
$(document).ready(function() {
  $('.ver-img-referencia').click(function() {
    var paramVerImgRefID = $(this).data('ver');
    var paramVerImgRefNombre = $(this).data('nombre');

    $.get("img-referencia/" + paramVerImgRefID, function(response, state) {
      console.log(paramVerImgRefID + " " + paramVerImgRefNombre);
      $("#img-ref-modal").attr("src", "http://codespipe.com/proyectos/insupomoplus-copia/" + response[0].imagen);
      $('#titulo-img-ref').html("Material" + " " + paramVerImgRefNombre);
      $('#modal1').openModal();
    });

  })
});
//===============================================================================
//Agregar insumo insupro
$(document).ready(function() {

  //===========================================================
  //Agrega piezas de insupros
  $('.exito-pieza').hide();
  $("#agregar-pieza").on('click', function() {

    if ($("#nombre-insupro-pieza").val() !== "" && $("#material-insupro-pieza").val() !== null && $("#color-insupro-pieza").val() !== null && $("#cantidad-pieza").val() !== "" && $("#largo-pieza").val() !== "" && $("#ancho-pieza").val() !== "") {
      agregraPieza();
    } else {
      alert("Complete todos los campos de Piezas");
    }
  });
  //===========================================================
  //Agrega insumos de insupros
  $('.exito-insumo-insupro').hide();
  $("#agregar-insumo-insupro").on('click', function() {
    if ($("#cantidad-insumo").val() !== "" && $("#insumo-insupro-id").val() !== "" && $("#consumo-insumo").val() !== null && $("#obser-insumo-textarea").val() !== "") {
      agregraInsumoInsupro();
    } else {
      alert("Complete todos los campos de Insumos");
    }
  });

  //Agrega oficios de insupros
  $('.exito-oficio').hide();
  $("#agregar-oficio-insupro").on('click', function() {
    if ($("#proceso-oficio").val() !== "" && $("#oficios-id").val() !== null && $("#cantidad-oficio").val() !== "") {
      agregraOficiosInsupro();
    } else {
      alert("Complete todos los campos de Oficios");
    }
  });

  //Agrega confecciones insupros
  $('.exito-confeccion').hide();
  $("#agregar-confeccion").on('click', function() {
    if ($("#proceso-confecciones").val() !== "" && $("#maquina-id").val() !== null && $("#recorrido-confecciones").val() !== '' && $("#cantidad-confecciones").val() !== "") {
      agregraConfeccionesInsupro();
    } else {
      alert("Complete todos los campos de Confecciones");
    }
  });

  //=====================================================
  $('#referencia-id-cotizacion').change(function(event) {
    var valReferencia = $("#referencia-id-cotizacion option:selected").val();
    getValuesRefCotiza(valReferencia);
  })

  //=====================================================
  $("#btn-calcular").on('click', function() {
    var itemValorUno = parseFloat($("#item-valor-1").val());
    var itemValorDos = parseFloat($("#item-valor-2").val());
    var itemValorTres = parseFloat($("#item-valor-3").val());
    var itemValorCuatro = parseFloat($("#item-valor-4").val());
    var itemValorCinco = parseFloat($("#item-valor-5").val());
    var itemValorSeis = parseFloat($("#item-valor-6").val());

    console.log(itemValorUno);
    var suma = itemValorUno + itemValorDos + itemValorTres + itemValorCuatro + itemValorCinco + itemValorSeis;

    $("#calculo").html(suma);
  });

});

function agregraPieza() {
  var piezaNombreIn, piezaMatIn, piezaColorIn, piezaCantIn, piezaAnchoIn, piezaLargoIn, route, token, insupro;

  piezaNombreIn = $('#nombre-insupro-pieza').val();
  piezaMatIn = $('#material-insupro-piezas').val();
  piezaColorIn = $('#color-insupro-piezas').val();
  piezaCantIn = $('#cantidad-pieza').val();
  piezaAnchoIn = $('#ancho-pieza').val();
  piezaLargoIn = $('#largo-pieza').val();
  insupro = $('#agregar-pieza').data('insupro');
  route = "/proyectos/insupomoplus-copia/public/insupros/agregar-piezas/" + insupro;
  token = $('#token').val();
  return $.ajax({
    url: route,
    headers: {
      'X-CSRF-TOKEN': token
    },
    type: 'POST',
    dataType: 'json',
    data: {
      piezaNom: piezaNombreIn,
      piezaMat: piezaMatIn,
      piezaColor: piezaColorIn,
      piezaCant: piezaCantIn,
      piezaAncho: piezaAnchoIn,
      piezaLargo: piezaLargoIn
    },
    success: function(response, textStatus, jqXHR) {
      //====================================================
      console.log(route);
      console.log(response.idPieza);
      $('ul.tabs').tabs();
      $("#piezas-tbody").prepend("<tr id='pieza-" + response.idPieza + "'class='center-align'><td  class='center-align'>" + $("#nombre-insupro-pieza").val() + "</td><td  class='center-align'>" + $("#material-insupro-piezas").val() + "</td><td  class='center-align'>" + $("#color-insupro-piezas").val() + "</td><td  class='center-align'>" + $("#cantidad-pieza").val() + "</td><td  class='center-align'>" + $("#ancho-pieza").val() + "</td><td  class='center-align'>" + $("#largo-pieza").val() + "</td><td class='center-align'><button type='button' data-id-insu='" + response.idPieza + "'class='waves-effect waves-light btn red darken-4'><i class='material-icons'>cancel</i></button></td></tr>");
      $(".exito-pieza").html("La pieza" + " " + piezaNombreIn + " " + "se ha creado con éxito");
      $(".exito-pieza").slideDown();
    },
    error: function(response) {
      console.log("Error" + " " + response);

    }
  });
}
//========================================= Piezas insupro

function agregraInsumoInsupro() {
  var cantidadInsumoIn, tokenInsumos, insumoInsuproIn, consumoInsumoIn, observacionInsuproIn, insupro, route;

  cantidadInsumoIn = $('#cantidad-insumo').val();
  insumoInsuproIn = $('#insumo-insupro-id').val();
  consumoInsumoIn = $('#consumo-insumo').val();
  observacionInsuproIn = $('#obser-insumo-textarea').val();
  insupro = $('#agregar-insumo-insupro').data('insupro');
  route = "/proyectos/insupomoplus-copia/public/insupros/agregar-insumos-insupro/" + insupro;
  tokenInsumos = $('#token-insumos').val();
  return $.ajax({
    url: route,
    headers: {
      'X-CSRF-TOKEN': tokenInsumos
    },
    type: 'POST',
    dataType: 'json',
    data: {
      cantidadInsumo: cantidadInsumoIn,
      insumoInsupro: insumoInsuproIn,
      consumoInsumo: consumoInsumoIn,
      observacionInsupro: observacionInsuproIn

    },
    success: function(response, textStatus, jqXHR) {
      //====================================================
      console.log(route);
      console.log(response.idInsumoInsupro);
      $('ul.tabs').tabs();
      $("#insumos-tbody").prepend("<tr id='insumo-insupro-" + response.idInsumoInsupro + "'class='center-align'><td  class='center-align'>" + $("#cantidad-insumo").val() + "</td><td  class='center-align'>" + $("#insumo-insupro-id").val() + "</td><td  class='center-align'>" + $("#consumo-insumo").val() + "</td><td  class='center-align'>" + $("#obser-insumo-textarea").val() + "</td><td class='center-align'><button type='button' data-id-insu='" + response.idInsumoInsupro + "'class='waves-effect waves-light btn red darken-4'><i class='material-icons'>cancel</i></button></td></tr>");
      $(".exito-insumo-insupro").html("La pieza" + " " + insumoInsuproIn + " " + "se ha creado con éxito");
      $(".exito-insumo-insupro").slideDown();
    },
    error: function(response) {

    }
  });
}

function agregraOficiosInsupro() {
  var procesoOficioIn, oficiosIdIn, cantidadOficioIn, insupro, route, tokenOficios;

  procesoOficioIn = $("#proceso-oficio").val();
  oficiosIdIn = $("#oficios-id").val();
  cantidadOficioIn = $('#cantidad-oficio').val();
  insupro = $('#agregar-insumo-insupro').data('insupro');
  route = "/proyectos/insupomoplus-copia/public/insupros/agregar-oficios-varios-insupro/" + insupro;
  tokenOficios = $('#token-oficios').val();

  return $.ajax({
    url: route,
    headers: {
      'X-CSRF-TOKEN': tokenOficios
    },
    type: 'POST',
    dataType: 'json',
    data: {
      procesoOficio: procesoOficioIn,
      oficiosId: oficiosIdIn,
      cantidadOficio: cantidadOficioIn
    },
    success: function(response, textStatus, jqXHR) {
      //====================================================
      console.log(route);
      console.log(response.idOficioInsupro);
      $('ul.tabs').tabs();
      $("#oficio-tbody").prepend("<tr id='oficio-insupro-" + response.idOficioInsupro + "'class='center-align'><td  class='center-align'>" + procesoOficioIn + "</td><td  class='center-align'>" + oficiosIdIn + "</td><td  class='center-align'>" + cantidadOficioIn + "</td><td class='center-align'><button type='button' data-id-insu='" + response.idOficioInsupro + "'class='waves-effect waves-light btn red darken-4'><i class='material-icons'>cancel</i></button></td></tr>");
      $(".exito-oficio").html("El oficio" + " " + "se ha creado con éxito");
      $(".exito-oficio").slideDown();
    },
    error: function(response) {

    }
  });
}

function agregraConfeccionesInsupro() {
  var procesoConfeccionesIn, maquinaIdIn, cantidadConfeccionesIn, recorridoConfeccionIn, insupro, route, tokenOficios;

  procesoConfeccionesIn = $("#proceso-confecciones").val();
  maquinaIdIn = $("#maquina-id").val();
  recorridoConfeccionIn = $('#recorrido-confecciones').val();
  cantidadConfeccionesIn = $('#cantidad-confecciones').val();
  insupro = $('#agregar-insumo-insupro').data('insupro');
  route = "/proyectos/insupomoplus-copia/public/insupros/agregar-confecciones-insupro/" + insupro;
  tokenOficios = $('#token-confecciones').val();

  return $.ajax({
    url: route,
    headers: {
      'X-CSRF-TOKEN': tokenOficios
    },
    type: 'POST',
    dataType: 'json',
    data: {
      procesoConfecciones: procesoConfeccionesIn,
      maquinaId: maquinaIdIn,
      recorridoConfeccion: recorridoConfeccionIn,
      cantidadConfecciones: cantidadConfeccionesIn
    },
    success: function(response, textStatus, jqXHR) {
      //====================================================
      console.log(route);
      console.log(response.idConfeccionesInsupro);
      $('ul.tabs').tabs();
      $("#confeccion-tbody").prepend("<tr id='confeccion-insupro-" + response.idConfeccionesInsupro + "'class='center-align'><td  class='center-align'>" + procesoConfeccionesIn + "</td><td  class='center-align'>" + maquinaIdIn + "</td><td  class='center-align'>" + recorridoConfeccionIn + "</td><td  class='center-align'>" + cantidadConfeccionesIn + "</td><td class='center-align'><button type='button' data-id-insu='" + response.idConfeccionesInsupro + "'class='waves-effect waves-light btn red darken-4'><i class='material-icons'>cancel</i></button></td></tr>");
      $(".exito-confeccion").html("La confección" + " " + "se ha creado con éxito");
      $(".exito-confeccion").slideDown();
    },
    error: function(response) {

    }
  });
}


function getValuesRefCotiza(referenciaId) {
  var route;
  route = "/proyectos/insupomoplus-copia/public/cotizaciones/valores-referencia/" + referenciaId;
  $.get(route, function(response, state) {
    console.log(response[0].length);
    for (var i = 0; i < response[0].length; i++) {
      $("#piezas-tbody").prepend("<tr 'class='center-align'><td  class='center-align'>" + response[0][i].nombrePieza + "</td><td  class='center-align'>" + response[0][i].nombreMaterial + "</td><td  class='center-align'>" + response[0][i].nombreColor + "</td><td  class='center-align'>" + response[0][i].cantidad + "</td><td  class='center-align'>" + response[0][i].ancho + "</td><td  class='center-align'>" + response[0][i].largo + "</td></tr>");
    };

    for (var i = 0; i < response[1].length; i++) {
      $("#insumos-tbody").prepend("<tr 'class='center-align'><td  class='center-align'>" + response[1][i].nombreInsumo + "</td><td  class='center-align'>" + response[1][i].cantidad + "</td><td  class='center-align'>" + response[1][i].consumo + "</td><td  class='center-align'>" + response[1][i].observaciones + "</td></tr>");
    };
    for (var i = 0; i < response[2].length; i++) {
      $("#confeccion-tbody").prepend("<tr 'class='center-align'><td  class='center-align'>" +response[2][i].proceso_confecciones + "</td><td  class='center-align'>" + response[2][i].nombreMaquina + "</td><td  class='center-align'>" + response[2][i].recorrido + "</td><td  class='center-align'>" + response[2][i].cantidad + "</td></tr>");
    };
     for (var i = 0; i < response[3].length; i++) {
      $("#oficio-tbody").prepend("<tr 'class='center-align'><td  class='center-align'>" + response[3][i].proceso_oficio+ "</td><td  class='center-align'>" + response[3][i].nombreOficio + "</td><td  class='center-align'>" + response[3][i].cantidad + "</td></tr>");
    };
  });
}
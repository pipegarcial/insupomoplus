<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CampoFactorRequest;
use App\Entities\Oficio;
use Session;
use Redirect;
use DB;

class OficioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oficio = Oficio:: 
        select('*')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->paginate(20);

        return view('oficios.oficio',['oficios' => $oficio]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('oficios.create-oficio');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         DB::table('oficios')->insert([
            'nombre' => $request['nombre'],
            'factor' => $request['factor']
        ]);
        return redirect('oficios/create')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $oficio = Oficio::find($id);
        return view('oficios.edit-oficio',['oficios'=>$oficio]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CampoFactorRequest $request, $id)
    {
        $oficio = Oficio::find($id);
        $oficio->fill($request->all());
        $oficio->save();

        Session::flash('message','El oficio ha sido editada correctamente');
        return Redirect::to('/oficios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

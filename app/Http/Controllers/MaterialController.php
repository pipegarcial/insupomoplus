<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Material;
use App\Entities\Proveedor;
use App\Helpers\HelperFunctions;
use App\Http\Requests\MaterialRequest;
use Input;
use Session;
use Redirect;
use DB;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $material = Material:: 
        select('*')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->paginate(20);

        return view('materiales.material',['materiales' => $material]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedor = Proveedor:: 
        select('empresa','id')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->get();

        
        return view('materiales.create-material',['proveedores'=>$proveedor]);
    }
    
  /**
     * Retrive specific info.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function getProveedoresMateriales(Request $request,$tabla,$tablaRel,$colIdRel,$id){
      $hProveedorMaterial = new HelperFunctions();
      $proveedorLista =  $hProveedorMaterial->getProveedoresTabla($tabla,$id,$tablaRel,$colIdRel);

        if($request->ajax()){
            return response()->json($proveedorLista);
        }else{
          return $tabla." ".$tablaRel." ".$colIdRel." ".$id;
        }
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MaterialRequest $request)
    {
        // Registra la información del material
        DB::table('materiales')->insert([
            'nombre' => $request['nombre'],
            'precio' => $request['precio'],
            'ancho'  => $request['ancho']
        ]);
        //--------------------------------------------------
        //--------------------------------------------------
        $hProveedorMaterial = new HelperFunctions();
        $idMaterial = $hProveedorMaterial->getLastIdTable('materiales')->id;
        //Verifica si existe la variable de proveedores, en caso de
        if(isset($request['proveedores-list'])){
            $hProveedorMaterial->insertProveedoresTabla('material_proveedores',$idMaterial,$request['proveedores-list'],'materiales','id_material');
        }
        //--------------------------------------------------
        return redirect('materiales/create')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function getProveedoresLaminasDinamico(Request $request,$palabra){
      $hProveedorMaterial = new HelperFunctions();
      $proveedorLista =  $hProveedorMaterial->getProveedoresTablaDinamico($palabra);

        if($request->ajax()){
            return response()->json($proveedorLista);
        }else{
          return "Error";
        }
    }
  
  
   public function getProveedoresLaminasDinamicoEdit(Request $request,$id,$palabra){
      $hProveedorMaterial = new HelperFunctions();
      $proveedorLista =  $hProveedorMaterial->getProveedoresTablaDinamico($palabra);

        if($request->ajax()){
            return response()->json($proveedorLista);
        }else{
          return "Error";
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proveedor = Proveedor:: 
        select('empresa','id')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->get();

        $hProveedorMaterial = new HelperFunctions();
        $proveedorLista =  $hProveedorMaterial->getProveedoresTabla('materiales',$id,'material_proveedores','id_material');

        $material = Material::find($id);

        return view('materiales.edit-material',['material'=>$material,'proveedoresList'=>$proveedorLista,'proveedores'=>$proveedor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $material = Material::find($id);
        $material->fill($request->all());
        $material->save();

        //--------------------------------------------------
        //--------------------------------------------------

        $hProveedorMaterial = new HelperFunctions();
        //Verifica si existe la variable de proveedores, en caso de
        if(isset($request['proveedores-list'])){
            $hProveedorMaterial->insertProveedoresTabla('material_proveedores',$id,$request['proveedores-list'],'materiales','id_material');
        }

        Session::flash('message','El material ha sido editada correctamente');
        return Redirect::to('/materiales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

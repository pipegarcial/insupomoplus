<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\HelperFunctions;
use App\Http\Requests;
use App\Entities\Material;
use App\Entities\Color;
use App\Entities\Maquina;
use App\Entities\Insumo;
use App\Entities\Oficio;
use App\Entities\Insupro;
use App\Entities\Referencia;
use App\Entities\Pieza;
use App\Entities\InsumosInsupro;
use App\Entities\OficiosVariosInsupro;
use App\Http\Controllers\Controller;
use App\Entities\ConfeccionesInsupro;
use App\Http\Requests\InsuproRequest;
use App\Http\Requests\PiezasRequest;
use Session;
use Redirect;


use DB;

class InsuproController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $insupro = Insupro:: 
      select('insupros.id','insupros.nombre','insupros.creador','referencias.nombre as refNombre','insupros.id','insupros.updated_at')
      ->join('referencias', 'referencias.id', '=', 'insupros.referencia')
      ->where('insupros.estado',1)        
      ->orderBy('insupros.id', 'asc')
      ->paginate(20);



      return view('insupros.insupro',['insupros' => $insupro]);
      return view('insupros.insupro');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {   
      $referencias = Referencia:: 
      select('nombre','id')
      ->where('estado',1)        
      ->orderBy('id', 'asc')
      ->get();

      return view('insupros.create-insupro',['referencias'=>$referencias]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(InsuproRequest $request)
  {
      DB::table('insupros')->insert([
          'nombre'      => $request['nombre'],
          'creador'      => 'admin',
          'referencia'      => $request['referencia'],
      ]);
    $lastId = 1;
      return redirect("insupros/{$lastId}/edit")->with('message','store');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //---------------------------
     //Insumo
     $insumos = Insumo:: 
      select('nombre','id')
      ->where('estado',1)        
      ->orderBy('id', 'asc')
      ->get();
      //---------------------------
      //Material
      $material = Material:: 
      select('nombre','id')
      ->where('estado',1)        
      ->orderBy('id', 'asc')
      ->get();
      //---------------------------
      //---------------------------
      //Color
      $color = Color:: 
      select('nombre','id')
      ->where('estado',1)        
      ->orderBy('id', 'asc')
      ->get();
      //---------------------------
      //Maquina
      $maquina = Maquina:: 
      select('nombre','id')
      ->where('estado',1)        
      ->orderBy('id', 'asc')
      ->get();
      //---------------------------
      //---------------------------
      //Oficio
      $oficio = Oficio:: 
      select('nombre','id')
      ->where('estado',1)        
      ->orderBy('id', 'asc')
      ->get();
   
      //---------------------------
      //
      $referencias = Referencia:: 
      select('nombre','id')
      ->where('estado',1)        
      ->orderBy('id', 'asc')
      ->get();

      $referenciasEspecifica = Insupro:: 
      select('referencias.nombre')
      ->join('referencias', 'referencias.id', '=', 'insupros.referencia')  
      ->where('insupros.estado',1)
      ->where('insupros.id',$id)       
      ->first();

      $piezas = Pieza:: 
      select('id','nombre','material_id','color_id','cantidad','ancho','largo')
      ->where('estado',1)
     ->where('insupro_id',$id)  
      ->orderBy('id', 'asc')
      ->get();
      //-------
      $insumos_insupros = InsumosInsupro:: 
      select('id','insumo_id','cantidad','consumo','observaciones')
      ->where('estado',1)
     ->where('insupro_id',$id)  
      ->orderBy('id', 'asc')
      ->get();
      //-------
      $confecciones_insupros = ConfeccionesInsupro:: 
      select('id','proceso_confecciones','maquina_id','recorrido','cantidad')
      ->where('estado',1)
     ->where('insupro_id',$id)  
      ->orderBy('id', 'asc')
      ->get();
      //-------
      $oficios_insupros = OficiosVariosInsupro:: 
      select('id','proceso_oficio','oficio_id','cantidad')
      ->where('estado',1)
     ->where('insupro_id',$id)  
      ->orderBy('id', 'asc')
      ->get();

      $insupro = Insupro::find($id);
    
      return view('insupros.edit-insupro',['referenciasEspecifica'=>$referenciasEspecifica,'referencias'=>$referencias,'insupro'=>$insupro,'insumos'=>$insumos,'materiales'=>$material,'oficios'=>$oficio,'maquinas'=>$maquina,'colores'=>$color,'piezas'=>$piezas,'insumos_insupros'=>$insumos_insupros,'confecciones'=>$confecciones_insupros,'oficiosInsupro'=>$oficios_insupros]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      $insupro = Insupro::find($id);
      $insupro->fill($request->all());
      $insupro->save();

      //Session::flash('message','El elemento de la categoría insumos ha sido editada correctamente');
    return redirect("insupros/{$id}/edit")->with('message','store');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }

  //===================================================================
 public function guardarPiezasInsupro(PiezasRequest $request,$idInsupro)
  {
       if($request->ajax()){
          DB::table('piezas_insupro')->insert([
          'nombre' => $request['piezaNom'],
          'material_id' => $request['piezaMat'],
          'color_id' => $request['piezaColor'],
          'cantidad' => $request['piezaCant'],
          'ancho' => $request['piezaAncho'],
          'largo' => $request['piezaLargo'],
          'insupro_id' => $idInsupro

      ]);

     $helper = new HelperFunctions();
     $lastIdRef = $helper->contarDatos('piezas_insupro')>0 ? ($helper->getLastIdTable('piezas_insupro')->id): 1;

       return response()->json([
          "idPieza"=>$lastIdRef
       ]);
      }

  }

  //===================================================================
   public function guardarInsumosInsupro(Request $request,$idInsupro)
  {
       if($request->ajax()){
          DB::table('insumos_insupro')->insert([
          'insumo_id' => $request['insumoInsupro'],
          'cantidad' => $request['cantidadInsumo'],
          'consumo' => $request['consumoInsumo'],
          'insupro_id' => $idInsupro,
          'observaciones' => $request['observacionInsupro'],


      ]);

     $helper = new HelperFunctions();
     $lastIdRef = $helper->contarDatos('insumos_insupro')>0 ? ($helper->getLastIdTable('insumos_insupro')->id): 1;


       return response()->json([
          "idInsumoInsupro"=>$lastIdRef
       ]);
      }

  }
  
    //===================================================================
   public function guardarConfeccionesInsupro(Request $request,$idInsupro)
  {
       if($request->ajax()){
          DB::table('confecciones_insupro')->insert([
          'proceso_confecciones' => $request['procesoConfecciones'],
          'maquina_id' => $request['maquinaId'],
          'cantidad' => $request['cantidadConfecciones'],
           'insupro_id' => $idInsupro,
          'recorrido' => $request['cantidadConfecciones'],


      ]);

     $helper = new HelperFunctions();
     $lastIdRef = $helper->contarDatos('insumos_insupro')>0 ? ($helper->getLastIdTable('insumos_insupro')->id): 1;


       return response()->json([
          "idConfeccionesInsupro"=>$lastIdRef
       ]);
      }

  }
  
  
    //===================================================================
   public function guardarOficiosInsupro(Request $request,$idInsupro)
  {
       if($request->ajax()){
          DB::table('oficios_varios_insupro')->insert([
          'proceso_oficio' => $request['procesoOficio'],
          'oficio_id' => $request['oficiosId'],
          'cantidad' => $request['cantidadOficio'],
          'insupro_id' => $idInsupro,
      ]);

     $helper = new HelperFunctions();
     $lastIdRef = $helper->contarDatos('insumos_insupro')>0 ? ($helper->getLastIdTable('insumos_insupro')->id): 1;

       return response()->json([
          "idOficioInsupro"=>$lastIdRef
       ]);
      }

  }
}

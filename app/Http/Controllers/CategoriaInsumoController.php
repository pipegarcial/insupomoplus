<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CamposCategoriasRequest;
use App\Entities\CategoriaElemento;
use Session;
use Redirect;
use DB;

class CategoriaInsumoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoriaInsumo = CategoriaElemento:: 
        select('*')
        ->where('estado',1)
        ->where('categoria_id',1)         
        ->orderBy('id', 'asc')
        ->paginate(20);

        return view('categoria_insumos.categoria_insumo',['categoriaInsumos' => $categoriaInsumo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categoria_insumos.create-categoria_insumo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CamposCategoriasRequest $request)
    {
        DB::table('categoria_elementos')->insert([
            'codigo' => $request['codigo'],
            'nombre' => $request['nombre'],
            'categoria_id' => 1
           
        ]);
        return redirect('categoria-insumo/create')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoriaInsumo = CategoriaElemento::find($id);
        return view('categoria_insumos.edit-categoria_insumo',['categoriaInsumo'=>$categoriaInsumo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoriaInsumo = CategoriaElemento::find($id);
        $categoriaInsumo->fill($request->all());
        $categoriaInsumo->save();

        Session::flash('message','El elemento de la categoría insumos ha sido editada correctamente');
        return Redirect::to('categoria-insumo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

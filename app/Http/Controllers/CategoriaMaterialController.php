<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CamposCategoriasRequest;
use App\Entities\CategoriaElemento;
use Session;
use Redirect;
use DB;

class CategoriaMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoriaMaterial = CategoriaElemento:: 
        select('*')
        ->where('estado',1)
        ->where('categoria_id',2)          
        ->orderBy('id', 'asc')
        ->paginate(20);

        return view('categoria_materiales.categoria_material',['categoriaMateriales' => $categoriaMaterial]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categoria_materiales.create-categoria_material');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CamposCategoriasRequest $request)
    {
        DB::table('categoria_elementos')->insert([
            'codigo' => $request['codigo'],
            'nombre' => $request['nombre'],
            'categoria_id' => 2
        ]);
        return redirect('categoria-material/create')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoriaMaterial = CategoriaElemento::find($id);
        return view('categoria_materiales.edit-categoria_material',['categoriaMaterial'=>$categoriaMaterial]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoriaMaterial = CategoriaElemento::find($id);
        $categoriaMaterial->fill($request->all());
        $categoriaMaterial->save();

        Session::flash('message','El elemento de la categoría material ha sido editada correctamente');
        return Redirect::to('/categoria-material');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

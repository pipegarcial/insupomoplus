<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\InsumoRequest;
use App\Http\Requests\InsumoUpdateRequest;
use App\Entities\Insumo;
use App\Entities\Proveedor;
use App\Entities\CategoriaElemento;
use App\Helpers\HelperFunctions;
use Session;
use Redirect;
use DB;

class InsumoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insumos = Insumo:: 
        select('insumos.id','insumos.nombre','insumos.nombre as insnombre','insumos.precio','categoria_elementos.nombre as catnombre','insumos.updated_at')
        ->join('categoria_elementos', 'categoria_elementos.id', '=', 'insumos.categoria_insumo_id')
        ->where('insumos.estado',1)       
        ->orderBy('id', 'asc')
        ->paginate(20);
      
      
        return view('insumos.insumo',['insumos' => $insumos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedor = Proveedor:: 
        select('empresa','id')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->get();
      
        $categoriaInsumo = CategoriaElemento:: 
        select('nombre','id')
        ->where('estado',1)
        ->where('categoria_id',1)
        ->orderBy('id', 'asc')
        ->get();
        return view('insumos.create-insumo',['proveedores'=>$proveedor,'categoriaInsumo'=>$categoriaInsumo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsumoRequest $request)
    {
        DB::table('insumos')->insert([
            'nombre'  => $request['nombre'],
            'categoria_insumo_id' => $request['categoria_insumo_id'],
            'precio'   => $request['precio'],
        ]);
      
         //--------------------------------------------------
        //--------------------------------------------------
        $hProveedorMaterial = new HelperFunctions();
        $idInsumo = $hProveedorMaterial->getLastIdTable('insumos')->id;
        //Verifica si existe la variable de proveedores, en caso de
        if(isset($request['proveedores-list'])){
          $hProveedorMaterial->insertProveedoresTabla('insumos_proveedores',$idInsumo,$request['proveedores-list'],'insumos','id_insumo');
        }
        return redirect('insumos/create')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proveedor = Proveedor:: 
        select('empresa','id')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->get();
      
       $categoriaInsumo = CategoriaElemento:: 
        select('nombre','id')
        ->where('estado',1)
        ->where('categoria_id',1)
        ->orderBy('id', 'asc')
        ->get();
        
        //Helper
        $hProveedorMaterial = new HelperFunctions();
        $proveedorLista =  $hProveedorMaterial->getProveedoresTabla('insumos',$id,'insumos_proveedores','id_insumo');
        //-----------------------------------------------------------------
        $insumo = Insumo::find($id);
        
        return view('insumos.edit-insumo',['insumos'=>$insumo,'categoriaInsumo'=>$categoriaInsumo,'proveedoresList'=>$proveedorLista,'proveedores'=>$proveedor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InsumoUpdateRequest $request, $id)
    {
        $insumo = Insumo::find($id);
        $insumo->fill($request->all());
        $insumo->save();
      
        $hProveedorInsumos = new HelperFunctions();
        //Verifica si existe la variable de proveedores, en caso de
        if(isset($request['proveedores-list'])){
            $hProveedorInsumos->insertProveedoresTabla('insumos_proveedores',$id,$request['proveedores-list'],'insumos','id_insumo');
        }

        Session::flash('message','El insumo ha sido editada correctamente');
        return Redirect::to('/insumos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
  
      public function getProveedoresLaminasDinamico(Request $request,$palabra){
      $hProveedorMaterial = new HelperFunctions();
      $proveedorLista =  $hProveedorMaterial->getProveedoresTablaDinamico($palabra);

        if($request->ajax()){
            return response()->json($proveedorLista);
        }else{
          return "Error";
        }
    }
  
  
   public function getProveedoresLaminasDinamicoEdit(Request $request,$id,$palabra){
      $hProveedorMaterial = new HelperFunctions();
      $proveedorLista =  $hProveedorMaterial->getProveedoresTablaDinamico($palabra);

        if($request->ajax()){
            return response()->json($proveedorLista);
        }else{
          return "Error";
        }
    }

   /**
     * Retrive specific info.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function getProveedoresInsumos(Request $request,$tabla,$tablaRel,$colIdRel,$id){
      $hProveedorInsumos = new HelperFunctions();
      $proveedorLista =  $hProveedorInsumos->getProveedoresTabla($tabla,$id,$tablaRel,$colIdRel);

        if($request->ajax()){
            return response()->json($proveedorLista);
        }else{
          return $tabla." ".$tablaRel." ".$colIdRel." ".$id;
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\MaquinaRequest;
use App\Entities\Maquina;
use Session;
use Redirect;
use DB;

class MaquinaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maquina = Maquina:: 
        select('*')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->paginate(20);

        return view('maquinas.maquina',['maquinas' => $maquina]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('maquinas.create-maquina');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MaquinaRequest $request)
    {
        DB::table('maquinas')->insert([
            'nombre' => $request['nombre'],
            'factor' => $request['factor']
        ]);
        return redirect('maquinas/create')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $maquina = Maquina::find($id);
        return view('maquinas.edit-maquina',['maquinas'=>$maquina]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MaquinaRequest $request, $id)
    {
        $maquina = Maquina::find($id);
        $maquina->fill($request->all());
        $maquina->save();

        Session::flash('message','La maquina ha sido editada correctamente');
        return Redirect::to("maquinas/{$id}/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

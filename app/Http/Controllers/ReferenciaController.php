<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\HelperFunctions;
use App\Entities\Referencia;
use App\Entities\Material;
use App\Entities\Cliente;
use App\Entities\Color;
use App\Entities\Maquina;
use App\Entities\Oficio;
use App\Entities\Insupro;
use App\Entities\CategoriaElemento;
use App\Http\Requests\ReferenciaRequest;
use Input;
use Session;
use Redirect;
use DB;

class ReferenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $referencia = Referencia:: 
        select('referencias.id','referencias.nombre','categoria_elementos.nombre as catnombre','referencias.material_id','referencias.consecutivo','referencias.troquel','referencias.precio_sugerido','referencias.updated_at')
         ->join('categoria_elementos', 'categoria_elementos.id', '=', 'referencias.categoria_id')
        ->where('referencias.estado',1)        
        ->orderBy('id', 'asc')
        ->paginate(20);

        return view('referencias.referencia',['referencias'=>$referencia]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         //---------------------------
        $categoriaReferencia = CategoriaElemento:: 
        select('id','nombre')
        ->where('estado',1)
        ->where('categoria_id',3)        
        ->orderBy('id', 'asc')
        ->get();
        //---------------------------
        $material = Material:: 
        select('nombre','id')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->get();

        return view('referencias.create-referencia',['materiales'=>$material,'catReferencias'=>$categoriaReferencia]);
    }

    /**
     * Retrive specific info.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function getImagenReferencia(Request $request,$id){
      $urlImage = Referencia:: 
        select('imagen')
        ->where('id',$id)        
        ->get();

        //return $urlImage;
        if($request->ajax()){
            return response()->json($urlImage);
        }
    }

  /*
    public function getCategoriasElementos(Request $request,$id){

        $categoriaReferencia = CategoriaElemento:: 
        select('id','nombre')
        ->where('estado',1)
        ->where('categoria_id',$id)        
        ->orderBy('id', 'asc')
        ->get();

        if($request->ajax()){
            return response()->json($categoriaReferencia);
        }
    }*/
  
  public function getCodigoReferenciaElementos(Request $request,$id){
        $helper = new HelperFunctions();
    
        $categoriaReferencia =$helper->getEspecificValueColumn('codigo',$id)->codigo;
    
        $lastIdRef = $helper->contarDatos('referencias')>0 ? ($helper->getLastIdTable('referencias')->id)+1 : 1;
      
        //$lastIdRef = ($helper->getLastIdTable('referencias')->id)+1;
        if($request->ajax()){
            return response()->json($categoriaReferencia."-".$lastIdRef);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReferenciaRequest $request)
    {
      
      //=====================================================
      //Adding Image
      //=====================================================
      $urlImage = '/public/assets/img/uploads/referencia/';
      //Declarando nombre imagen-----------------------------
      $imageName = $request['consecutivo'] . '_' . $request['nombre'].
       ".".$request['imagen']->getClientOriginalExtension();
      //Guardando imagen en el servidor---------------------
      $request['imagen']->move(base_path() .  $urlImage, $imageName);
      //=====================================================
   
      // Registra la información del material
      DB::table('referencias')->insert([
        'consecutivo'      => $request['consecutivo'],
        'nombre'           => $request['nombre'],
        'material_id'      => $request['material_id'],
        'troquel'          => $request['troquel'],
        'precio_sugerido'  => $request['precio_sugerido'],
        'categoria_id'     => $request['categoria_id'],
        'largo'     => $request['largo'],
        'ancho'     => $request['ancho'],
        'profundidad'     => $request['profundidad'],
        'imagen'              => $urlImage . $imageName 
        ]);
        //--------------------------------------------------
        //--------------------------------------------------

        return redirect('referencias/create')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $referencia = Referencia::find($id);
        $categoriaReferencia = CategoriaElemento:: 
        select('id','nombre')
        ->where('estado',1)
        ->where('categoria_id',3)        
        ->orderBy('id', 'asc')
        ->get();
        //--------------------------
        //Get material data to sentd the referencia view
        $material = Material:: 
        select('nombre','id')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->get();
        //--------------------------

        return view('referencias.edit-referencia',['referencias'=>$referencia,'materiales'=>$material,'catReferencias'=>$categoriaReferencia]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      
      //=====================================================
      //Editando archivo
        $referencia = Referencia::find($id);
        $referencia->fill($request->all());
        $referencia->save();
      
      //=====================================================
      //=====================================================
      //Adding Image
      //=====================================================
      if(isset($request['imagen'])){
      $urlImage = '/public/assets/img/uploads/referencia/';
      //Declarando nombre imagen-----------------------------
      $imageName = $request['consecutivo'] . '_' . $request['nombre'].
       ".".$request['imagen']->getClientOriginalExtension();
      //Guardando imagen en el servidor---------------------
      $request['imagen']->move(base_path() .  $urlImage, $imageName);
      //=====================================================
              //Editando url 
      DB::table('referencias')
            ->where('id',  $id)
            ->update(['imagen'  => $urlImage . $imageName ]);
      }
    
      //=====================================================

      return Redirect::to("/referencias/{$id}/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
  
  
   public function getInsupros(Request $request,$id){
      $hProveedorMaterial = new HelperFunctions();
      $insupros =  $hProveedorMaterial->getInsupros($id);

        if($request->ajax()){
            return response()->json($insupros);
        }else{
          return  $insupros;
        }
    }
}

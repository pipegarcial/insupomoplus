<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CamposCategoriasRequest;
use App\Entities\CategoriaElemento;
use Session;
use Redirect;
use DB;

class CategoriaReferenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoriaReferencia = CategoriaElemento:: 
        select('*')
        ->where('estado',1)
        ->where('categoria_id',3)        
        ->orderBy('id', 'asc')
        ->paginate(20);

        return view('categoria_referencias.categoria_referencia',['categoriaReferencias' => $categoriaReferencia]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categoria_referencias.create-categoria_referencia');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CamposCategoriasRequest $request)
    {
        DB::table('categoria_elementos')->insert([
            'codigo' => $request['codigo'],
            'nombre' => $request['nombre'],
            'categoria_id' => 3
        ]);
        return redirect('categoria-referencia/create')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoriaReferencia = CategoriaElemento::find($id);
        return view('categoria_referencias.edit-categoria_referencia',['categoriaReferencia'=>$categoriaReferencia]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoriaReferencia = CategoriaElemento::find($id);
        $categoriaReferencia->fill($request->all());
        $categoriaReferencia->save();

        Session::flash('message','El elemento de la categoría referencias ha sido editada correctamente');
        return Redirect::to('/categoria-referencia');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

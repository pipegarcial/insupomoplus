<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CotizacionRequest;
use App\Entities\Referencia;
use App\Entities\Cliente;
use App\Entities\Insupro;
use App\Entities\Cotizacion;
use App\Entities\Pieza;
use App\Entities\InsumosInsupro;
use App\Entities\ConfeccionesInsupro;
use App\Entities\OficiosVariosInsupro;
use App\Http\Requests\InsuproRequest;

use Session;
use Redirect;
use DB;

class CotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cotizacion = Cotizacion:: 
        select('*')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->paginate(20);

        return view('cotizaciones.cotizacion',['cotizaciones' => $cotizacion]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //---------------------------------
      $clientes = Cliente:: 
      select('nombre','apellidos','id')
      ->where('estado',1)        
      ->orderBy('id', 'asc')
      ->get();
      //---------------------------------
      $referencias = Referencia:: 
      select('nombre','id')
      ->where('estado',1)        
      ->orderBy('id', 'asc')
      ->get();
      //---------------------------------
      
      return view('cotizaciones.create-cotizacion',['clientes' => $clientes,'referencias'=>$referencias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
  
  
    public function getValuesInsupro(Request $request,$idReferencia){
     /*$referencia = Referencia:: 
     select('referencias.id','referencias.nombre','categoria_elementos.nombre as catnombre','referencias.material_id','referencias.consecutivo','referencias.troquel','referencias.precio_sugerido','referencias.updated_at')
     ->join('categoria_elementos', 'categoria_elementos.id', '=', 'referencias.categoria_id')
     ->where('referencias.estado',1)        
     ->orderBy('id', 'asc')
     ->paginate(20);*/
      
      $insupro = Insupro:: 
      select('id','nombre')
      ->where('estado',1)
      ->where('referencia',$idReferencia)  
      ->orderBy('id', 'asc')
      ->get();
      
      
      $piezas = Pieza:: 
      select('piezas_insupro.id','piezas_insupro.nombre as nombrePieza','piezas_insupro.cantidad','piezas_insupro.ancho','piezas_insupro.largo','colores.nombre as nombreColor','materiales.nombre as nombreMaterial')
      ->join('colores', 'colores.id', '=', 'piezas_insupro.color_id')
      ->join('materiales', 'materiales.id', '=', 'piezas_insupro.material_id')
      ->where('piezas_insupro.estado',1)
      ->where('piezas_insupro.insupro_id',$insupro->first()->id)  
      ->orderBy('piezas_insupro.id', 'asc')
      ->get();
      //-------
      $insumos_insupros = InsumosInsupro:: 
      select('insumos_insupro.id','insumos.nombre as nombreInsumo','insumos_insupro.cantidad','insumos_insupro.consumo','insumos_insupro.observaciones')
      ->join('insumos', 'insumos.id', '=', 'insumos_insupro.insumo_id')
      ->where('insumos_insupro.estado',1)
     ->where('insumos_insupro.insupro_id',$insupro->first()->id)  
      ->orderBy('insumos_insupro.id', 'asc')
      ->get();
      //-------
      $confecciones_insupros = ConfeccionesInsupro:: 
      select('confecciones_insupro.proceso_confecciones','maquinas.nombre as nombreMaquina','confecciones_insupro.recorrido','confecciones_insupro.cantidad')
      ->join('maquinas', 'maquinas.id', '=', 'confecciones_insupro.maquina_id')
      ->where('confecciones_insupro.estado',1)
     ->where('confecciones_insupro.insupro_id',$insupro->first()->id)  
      ->orderBy('confecciones_insupro.id', 'asc')
      ->get();
      //-------
      $oficios_insupros = OficiosVariosInsupro:: 
      select('oficios_varios_insupro.proceso_oficio','oficios.nombre as nombreOficio','oficios_varios_insupro.cantidad')
     ->join('oficios', 'oficios.id', '=', 'oficios_varios_insupro.oficio_id')
      ->where('oficios_varios_insupro.estado',1)
     ->where('oficios_varios_insupro.insupro_id',$insupro->first()->id)  
      ->orderBy('oficios_varios_insupro.id', 'asc')
      ->get();
      
      if($request->ajax()){
         return response()->json([
        $piezas                 ->toArray(),
        $insumos_insupros       ->toArray(),
        $confecciones_insupros  ->toArray(),
        $oficios_insupros       ->toArray()
            ]);
      }
    
    }
}

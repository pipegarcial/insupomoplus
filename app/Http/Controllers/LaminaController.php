<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LaminaRequest;
use App\Http\Requests\LaminaUpdateRequest;
use App\Entities\Lamina;
use App\Entities\Proveedor;
use App\Helpers\HelperFunctions;
use Session;
use Redirect;
use DB;

class LaminaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $laminas = Lamina:: 
        select('*')
        ->where('estado',1)       
        ->orderBy('id', 'asc')
        ->paginate(20);
      
        return view('laminas.lamina',['laminas' => $laminas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedor = Proveedor:: 
        select('empresa','id')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->get();
        return view('laminas.create-lamina',['proveedores'=>$proveedor]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LaminaRequest $request)
    {
        DB::table('laminas')->insert([
            'nombre'  => $request['nombre'],
            'ancho'   => $request['ancho'],
            'largo'   => $request['largo'],
            'precio'  => $request['precio'],
        ]);
      
        //--------------------------------------------------
        //--------------------------------------------------
        $hProveedorMaterial = new HelperFunctions();
        $idLamina = $hProveedorMaterial->getLastIdTable('laminas')->id;
        //Verifica si existe la variable de proveedores, en caso de
        if(isset($request['proveedores-list'])){
          $hProveedorMaterial->insertProveedoresTabla('laminas_proveedores',$idLamina,$request['proveedores-list'],'laminas','id_lamina');
        }
        else{
         return "fallo";
        }
      
        return redirect('laminas/create')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proveedor = Proveedor:: 
        select('empresa','id')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->get();
        
        //Helper
        $hProveedorMaterial = new HelperFunctions();
        $proveedorLista =  $hProveedorMaterial->getProveedoresTabla('laminas',$id,'laminas_proveedores','id_lamina');
        //-----------------------------------------------------------------
        $lamina = Lamina::find($id);

        return view('laminas.edit-lamina',['lamina'=>$lamina,'proveedoresList'=>$proveedorLista,'proveedores'=>$proveedor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LaminaUpdateRequest $request, $id)
    {
        $lamina = Lamina::find($id);
        $lamina->fill($request->all());
        $lamina->save();
      
        $hProveedorLaminas = new HelperFunctions();
        //Verifica si existe la variable de proveedores, en caso de
        if(isset($request['proveedores-list'])){
            $hProveedorLaminas->insertProveedoresTabla('laminas_proveedores',$id,$request['proveedores-list'],'laminas','id_lamina');
        }

        Session::flash('message','La lamina ha sido editada correctamente');
        return Redirect::to('/laminas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
  
     public function getProveedoresLaminasDinamico(Request $request,$palabra){
      $hProveedorMaterial = new HelperFunctions();
      $proveedorLista =  $hProveedorMaterial->getProveedoresTablaDinamico($palabra);

        if($request->ajax()){
            return response()->json($proveedorLista);
        }else{
          return "Error";
        }
    }
  
  
   public function getProveedoresLaminasDinamicoEdit(Request $request,$id,$palabra){
      $hProveedorMaterial = new HelperFunctions();
      $proveedorLista =  $hProveedorMaterial->getProveedoresTablaDinamico($palabra);

        if($request->ajax()){
            return response()->json($proveedorLista);
        }else{
          return "Error";
        }
    }

  
   /**
     * Retrive specific info.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function getProveedoresLaminas(Request $request,$tabla,$tablaRel,$colIdRel,$id){
      $hProveedorMaterial = new HelperFunctions();
      $proveedorLista =  $hProveedorMaterial->getProveedoresTabla($tabla,$id,$tablaRel,$colIdRel);

        if($request->ajax()){
            return response()->json($proveedorLista);
        }else{
          return $tabla." ".$tablaRel." ".$colIdRel." ".$id;
        }
    }
}

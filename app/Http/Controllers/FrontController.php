<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FrontController extends Controller
{
    /**
     * Display a view for login.
     *
     * @return view login
    */
   
    public function login(){
        return view('login');
    }

    public function app(){
        return view('app');
    }

    public function admin(){
        return view('admin');
    }

    public function unidadesMedida(){
        return view('app.unidad_medida');
    }

}

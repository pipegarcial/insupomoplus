<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UnidadMedidaRequest;
use App\Http\Controllers\Controller;
use App\Entities\UnidadMedida;
use Auth;
use Session;
use Redirect;
use DB;

class UnidadMedidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$unidadMedida = DB::table('unidades_medida')
        ->orderBy('id', 'asc')
        ->select('*')
        ->where('estado',1)
        ->get();*/

        $unidadMedida = UnidadMedida::
        select('id','valor','created_at','updated_at')
        ->where('estado',1)        
        ->orderBy('id', 'asc')
        ->paginate(20);

        return view('unidades_medida.unidad-medida',['unidadesMedida' => $unidadMedida]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unidades_medida.create-unidad-medida');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UnidadMedidaRequest $request)
    {
        UnidadMedida::create([
            'valor'     => $request['valor']
        ]);

        return redirect('unidades-medida/create')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unidadMedida = UnidadMedida::find($id);
        return view('unidades_medida.edit-unidad-medida',['unidadMedida'=>$unidadMedida]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UnidadMedidaRequest $request, $id)
    {
        $unidadMedida = UnidadMedida::find($id);
        $unidadMedida->fill($request->all());
        $unidadMedida->save();

        Session::flash('message','La unidad de medida ha sido editada correctamente');
        return Redirect::to('/unidades-medida');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

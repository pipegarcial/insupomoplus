<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProveedorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'        =>'required|unique:proveedores,id',
            'empresa'   =>'required|unique:proveedores,empresa',
            'direccion' =>'required',
            'telefono'  =>'required',
            'celular'   =>'required',
            'email'     =>'required|email|unique:proveedores,email'
        ];
    }
}

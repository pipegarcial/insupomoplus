<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'usuario'                 => 'required|unique:users,usuario|between:5,20|alpha_num',
            'nombre'                  => 'required',  
            'apellido'                => 'required', 
            'password'                => 'required|Between:5,20',
            'password_confirmation'   => 'required|same:password',
        ];
    }
}

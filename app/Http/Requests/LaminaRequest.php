<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LaminaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' =>'required|unique:laminas,nombre',
            'ancho'  =>'required|numeric',
            'largo'  =>'required|numeric',
            'precio' =>'required|numeric'
        ];
    }
}

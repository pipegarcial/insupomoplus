<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ReferenciaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'consecutivo'   =>'required|unique:referencias,consecutivo',
            'nombre'        =>'required|unique:referencias,nombre',
            'material_id'      =>'required',
            'troquel'       =>'required',
            'precio_sugerido'   =>'required|numeric',
            'categoria_id'      =>'required|numeric',  
            'imagen'             =>'required'
        ];
    }
}
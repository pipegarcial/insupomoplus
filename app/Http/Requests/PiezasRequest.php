<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PiezasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'piezaNom'   =>'required|unique:piezas_insupro,nombre',
            'piezaMat'   =>'required',
            'piezaColor' =>'required',
            'piezaCant'  =>'required',
            'piezaAncho' =>'required',
            'piezaLargo' =>'required'
        ];
    }
}

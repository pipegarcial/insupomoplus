<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CamposCategoriasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo'     => 'required|unique:categoria_elementos,codigo',
            'nombre'     => 'required|unique:categoria_elementos,nombre',
        ];
    }
}

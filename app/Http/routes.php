<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/','FrontController@login');
Route::get('inicio','FrontController@app');
Route::get('logout','LoginController@logout');
//Route::get('unidades','FrontController@unidadesMedida');

//Route::get('crear-usuario','FrontController@createUsers');

Route::resource('login','LoginController');
Route::resource('user','UserController');
Route::resource('proveedores','ProveedorController');
Route::resource('clientes','ClienteController');
Route::resource('unidades-medida','UnidadMedidaController');
Route::resource('materiales','MaterialController');
Route::resource('marcas','MarcaController');
Route::resource('colores','ColorController');
Route::resource('oficios','OficioController');
Route::resource('maquinas','MaquinaController');
Route::resource('categoria-referencia','CategoriaReferenciaController');
Route::resource('categoria-insumo','CategoriaInsumoController');
Route::resource('categoria-material','CategoriaMaterialController');
Route::resource('referencias','ReferenciaController');
Route::resource('insupros','InsuproController');
Route::resource('laminas','LaminaController');
Route::resource('insumos','InsumoController');
Route::resource('cotizaciones','CotizacionController');

Route::get('referencias/codigo-elemento-referencia/{id}','ReferenciaController@getCodigoReferenciaElementos');
Route::get('proveedor-material/{tabla}/{tablaRel}/{colIdRel}/{id}','MaterialController@getProveedoresMateriales');
Route::get('proveedor-lamina/{tabla}/{tablaRel}/{colIdRel}/{id}','LaminaController@getProveedoresLaminas');
Route::get('proveedor-insumo/{tabla}/{tablaRel}/{colIdRel}/{id}','InsumoController@getProveedoresInsumos');
Route::get('img-referencia/{id}','ReferenciaController@getImagenReferencia');

Route::get('insumos/busqueda-proveedores-insumos/{palabra}','LaminaController@getProveedoresLaminasDinamico');
Route::get('insumos/{id}/busqueda-proveedores-insumos/{palabra}','LaminaController@getProveedoresLaminasDinamicoEdit');

Route::get('laminas/busqueda-proveedores-laminas/{palabra}','LaminaController@getProveedoresLaminasDinamico');
Route::get('laminas/{id}/busqueda-proveedores-laminas/{palabra}','LaminaController@getProveedoresLaminasDinamicoEdit');

Route::get('materiales/busqueda-proveedores-materiales/{palabra}','LaminaController@getProveedoresLaminasDinamico');
Route::get('laminas/{id}/busqueda-proveedores-laminas/{palabra}','LaminaController@getProveedoresLaminasDinamicoEdit');

Route::get('referencia-insupros/insupros/{id}','ReferenciaController@getInsupros');
//================================================================================
//Agregar elementos a las piezas
Route::post('insupros/agregar-piezas/{idInsupro}','InsuproController@guardarPiezasInsupro');
Route::post('insupros/agregar-insumos-insupro/{idInsupro}','InsuproController@guardarInsumosInsupro');
Route::post('insupros/agregar-confecciones-insupro/{idInsupro}','InsuproController@guardarConfeccionesInsupro');
Route::post('insupros/agregar-oficios-varios-insupro/{idInsupro}','InsuproController@guardarOficiosInsupro');

Route::get('cotizaciones/valores-referencia/{idReferencia}','CotizacionController@getValuesInsupro');









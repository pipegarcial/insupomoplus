<?php  

namespace App\Helpers;
use DB;
use App\Entities\CategoriaElemento;
class HelperFunctions
{

	/* 
	-------------------------------------------------
	Auxiliary functions
	-------------------------------------------------
	*/

    /**
     * Count if relation proveedor and material exist
     *
     * @param  int $idMaterial
     * @param  int  $idProveedor
     * @return $concept->total
     */
	public function countProveedorMaterial($idMaterial,$idProveedor,$tabla,$tablaRelacion,$col_id_rel){
		$proveedor = DB::table($tabla)
            ->join($tablaRelacion, $tablaRelacion.'.'.$col_id_rel, '=', $tabla.'.id')
            ->join('proveedores', 'proveedores.id', '=', $tablaRelacion.'.id_proveedor')
            ->select(DB::raw('count(*) as total'))
            ->where($tablaRelacion.'.'.$col_id_rel, $idMaterial)
            ->where($tablaRelacion.'.id_proveedor', $idProveedor)
            ->first();

        return $proveedor->total;
	}
    public function getProveedoresTablaDinamico($palabra){
			if(!empty($palabra) && $palabra !=='none' ){
				 $proveedor = DB::table('proveedores')
            ->select('id','empresa')
            ->where('estado',1)
						->where('proveedores.empresa','like',$palabra.'%')      
            ->get();
				

        return $proveedor;
			}
			else{
				 $proveedor = DB::table('proveedores')
            ->select('id','empresa')
            ->where('estado',1)
            ->get();
				
        return $proveedor;
			}
       
    }
	
	    public function getInsupros($id){
				 $insupros = DB::table('insupros')
            ->select('insupros.id','insupros.nombre')
					  ->join('referencias', 'referencias.id', '=', 'insupros.referencia')
            ->where('insupros.estado',1)
					  ->where('insupros.referencia',$id)
            ->get();
				

        return $insupros;
    }
	
    public function getProveedoresTabla($tabla,$idElemento,$tablaRelacion,$colIdRel){
        $proveedor = DB::table($tabla)
            ->join($tablaRelacion, $tablaRelacion.'.'.$colIdRel, '=', $tabla.'.id')
            ->join('proveedores', 'proveedores.id', '=', $tablaRelacion.'.id_proveedor')
            ->select('proveedores.id','proveedores.empresa','proveedores.direccion','proveedores.telefono','proveedores.celular','proveedores.email')
            ->where($tablaRelacion.'.'.$colIdRel, $idElemento)
            ->where($tablaRelacion.'.estado',1)        
            ->get();

        return $proveedor;
    }
	
	    public function getProveedoresMaterial($idMaterial){
        $proveedor = DB::table('materiales')
            ->join('material_proveedores', 'material_proveedores.id_material', '=', 'materiales.id')
            ->join('proveedores', 'proveedores.id', '=', 'material_proveedores.id_proveedor')
            ->select('proveedores.id','proveedores.empresa','proveedores.direccion','proveedores.telefono','proveedores.celular','proveedores.email')
            ->where('material_proveedores.id_material', $idMaterial)
            ->where('material_proveedores.estado',1)        
            ->get();

        return $proveedor;
    }

	/* 
	-------------------------------------------------
	Auxiliary functions
	-------------------------------------------------
	*/
    public function insertProveedoresTabla($table,$idElemento,$arrayProveedores,$tablaElemento,$idColElemento){
        foreach ($arrayProveedores as $proveedor) {
                if($this->countProveedorMaterial($idElemento,$proveedor,$tablaElemento,$table,$idColElemento)==0){
                    DB::table($table)->insert([
                        $idColElemento  => $idElemento,
                        'id_proveedor' => $proveedor,
                    ]);
                }
                else{
                    return "Ya existe";
                }
            }
    }

	/* 
	-------------------------------------------------
	Auxiliary functions
	-------------------------------------------------
	*/
	public function getLastIdTable($tabla){
		$idConcept = DB::table($tabla)
            ->select('id')
            ->orderBy('id', 'desc')
            ->take(1)
            ->first();

        return $idConcept;
    }
	
	public function getEspecificValueColumn($column,$id){
	$valueColum = CategoriaElemento:: 
        select('codigo')
        ->where('estado',1)
        ->where('id',$id)        
        ->orderBy('id', 'asc')
        ->first();
		return $valueColum;
	}
	
	
		public function contarDatos($tabla){
		$total = DB::table($tabla)
            ->select(DB::raw('count(*) as total'))
            ->where($tabla.'.'.'estado', 1)
            ->first();

        return $total->total;
	}

}



?>
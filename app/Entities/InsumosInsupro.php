<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class InsumosInsupro extends Model
{
    use Authenticatable, Authorizable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'insumos_insupro';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['insumo_id','cantidad','consumo','observaciones','insupro_id'];
}
  
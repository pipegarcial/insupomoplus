<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class ConfeccionesInsupro extends Model
{
    use Authenticatable, Authorizable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'confecciones_insupro';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['proceso','maquina_id','recorrido','cantidad','insupro_id'];
}
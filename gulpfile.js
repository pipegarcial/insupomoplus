var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

/*
 |--------------------------------Mix sass files
*/
elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix.sass([
        '**/*.scss',
    ], 	'public/assets/css');
});
//---------------------------------------------
//---------------------------------------------

/*
 |--------------------------------Mix js files
*/
elixir(function(mix) {
    mix.coffee([
    	'**/*.coffee'
    ],	'public/assets/js');
});
//---------------------------------------------
//---------------------------------------------

/*
 |--------------------------------Reload browser
*/
elixir(function(mix) {
    mix.browserSync();
});
//---------------------------------------------
//---------------------------------------------
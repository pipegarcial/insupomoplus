@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Editar Cat Material</h5>
        </div>

        <div class="col s12 center-align">
            {!! Form::model($color,array('route' => ['categoria-material.update',$color->id],'method'=>'PUT','class'=>'col s12')) !!}

               @include('categoria_materiales.include_form-categoria_material',['titleBtn' => 'Modificar']);

                {!! Form::close() !!}

    	</div>

        <div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso center-align">
            @if(Session::has('message'))
                <h6>Elemento categoría material editado </h6>       
            @endif      
        </div>
    </div>
@stop
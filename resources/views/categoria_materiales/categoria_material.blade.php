@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Categoría Material</h5>
        </div>


        <div class="s12 center-align">
           {!! link_to('categoria-material/create', $title = 'Agregar Elemento', $parameters = array("class" => "btn margin-btn-form"), 
       $attributes = array()) !!}
        </div>
    </div>
     <div class="row">
    	@if(Session::has('message'))
     		<div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso">
               {{Session::get('message')}}           
        	</div>
        @endif 
	
		<div class="col s12 center-align">
           <h6 class="bold">Cantidad registros: {!! $categoriaMateriales->total() !!}</h6>
    		{!! $categoriaMateriales->render() !!}
        </div>
     
		
		<div class="col s12 ">
	        <table>
		        <thead >
		          	<tr>
			            <th data-field="id"  class="center-align">Código</th>
			            <th data-field="name"  class="center-align">Código</th>
			            <th data-field="name"  class="center-align">Nombre</th>
			            <th data-field="price"  class="center-align">Fecha creación</th>
			            <th data-field="price"  class="center-align">Fecha Modificación</th>
			            <th data-field="price"  class="center-align">Modificar</th>
			            <th data-field="price"  class="center-align">Eliminar</th>
		          	</tr>
		        </thead>
			    <tbody>
			    	@foreach ($categoriaMateriales as $categoriaMaterial)
			          	<tr  class="center-align">
				            <td  class="center-align">{{$categoriaMaterial->id}}</td>
				            <td  class="center-align">{{$categoriaMaterial->codigo}}</td>
				            <td  class="center-align">{{$categoriaMaterial->nombre}}</td>
				            <td  class="center-align">{{$categoriaMaterial->created_at}}</td>
				            <td  class="center-align">{{$categoriaMaterial->updated_at}}</td>
				            <td  class="center-align">
							{!! link_to_route('categoria-material.edit', $title = 'Editar', $parameters = $categoriaMaterial->id, $attributes = array()) !!}

				            <i class="material-icons">mode_edit</i></td>
				            <td  class="center-align"><i class="material-icons">delete</i></td>
			          	</tr>
					@endforeach
			    </tbody>
	      </table>
      </div>
    </div>
    <div class="col s12 center-align">
        <h6 class="bold">Cantidad registros: {!! $categoriaMateriales->total() !!}</h6>
    	{!! $categoriaMateriales->render() !!}
    </div>
@stop
@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear Proveedor</h5>
        </div>

        <!-- ============================ -->
        @include('includes.include_mensaje-creado',['mensaje' => 'proveedor creado'])
        
        <!-- ============================ -->
        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'proveedores.store','method' => 'POST','class'=>'col s12')) !!}

                @include('proveedores.include_form-proveedor',['titleBtn' => 'Crear'])

                {!! Form::close() !!}

    	</div>
        <!-- ============================ -->
        @include('includes.include_mensaje-creado',['mensaje' => 'proveedor creado'])
    </div>
@stop
<div class="col s12 m12 l6 offset-l3 center-align">
    <div class="input-field col s12 center-align">
        {!! Form::text('id',null, array('class' => 'validate','id' => 'icon_prefix')) !!}
        {!! Form::label('icon_prefix', 'Nit...')  !!}

        {!! $errors->first('id', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="input-field col s12 center-align">
        {!! Form::text('empresa',null, array('class' => 'validate','id' => 'icon_prefix')) !!}
        {!! Form::label('icon_prefix', 'Empresa...')  !!}

        {!! $errors->first('empresa', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="input-field col s12 center-align">
        {!! Form::text('direccion',null, array('class' => 'validate','id' => 'icon_prefix')) !!}
        {!! Form::label('icon_prefix', 'Dirección...')  !!}

        {!! $errors->first('direccion', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="input-field col s12 center-align">
        {!! Form::text('telefono',null, array('class' => 'validate','id' => 'icon_prefix')) !!}
        {!! Form::label('icon_prefix', 'Teléfono...')  !!}

        {!! $errors->first('telefono', "<span class='error center-align'>:message</span>") !!}
    </div>

    
    <div class="input-field col s12 center-align">
        {!! Form::text('celular',null, array('class' => 'validate','id' => 'icon_prefix')) !!}
        {!! Form::label('icon_prefix', 'Célular...')  !!}

        {!! $errors->first('celular', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="input-field col s12 center-align">
        {!! Form::text('email',null, array('class' => 'validate','id' => 'icon_prefix')) !!}
        {!! Form::label('icon_prefix', 'Email...')  !!}

        {!! $errors->first('email', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="col s12 m12 l12 center-align">
        {!! Form::submit($titleBtn,array("class" => "btn margin-btn-form btn-40")) !!}
    </div>

    <div class="col s12 m12 l12 center-align">
       {!! link_to('proveedores', $title = 'Volver', $parameters = array("class" => "btn margin-btn-form btn-40"), 
       $attributes = array()) !!}
    </div>
</div>
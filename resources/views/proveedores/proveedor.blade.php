@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Proveedores</h5>
        </div>

        <div class="s12 center-align">
           {!! link_to('proveedores/create', $title = 'Agregar Proveedor', $parameters = array("class" => "btn margin-btn-form"), 
       $attributes = array()) !!}
        </div>
    </div>
     <div class="row">
    	@if(Session::has('message'))
     		<div class="col s12 center-align card-panel  light-green darken-1 exitoso">
               {{Session::get('message')}}           
        	</div>
        @endif 
	
		<div class="col s12 center-align">
           <h6 class="bold">Cantidad registros: {!! $proveedores->total() !!}</h6>
    		{!! $proveedores->render() !!}
        </div>
     
		
		<div class="col s12 ">
	        <table>
		        <thead >
		          	<tr>
			            <th data-field="id"  class="center-align">Nit</th>
			            <th data-field="name"  class="center-align">Empresa</th>
			            <th data-field="price"  class="center-align">Dirección</th>
			            <th data-field="price"  class="center-align">Teléfono</th>
			            <th data-field="price"  class="center-align">Célular</th>
			            <th data-field="price"  class="center-align">Email</th>
			            <th data-field="price"  class="center-align">Fecha creación</th>
			            <th data-field="price"  class="center-align">Fecha Modificación</th>
			            <th data-field="price"  class="center-align">Modificar</th>
			            <th data-field="price"  class="center-align">Eliminar</th>
		          	</tr>
		        </thead>
			    <tbody>
			    	@foreach ($proveedores as $proveedor)
			          	<tr  class="center-align">
				            <td  class="center-align">{{$proveedor->id}}</td>
				            <td  class="center-align">{{$proveedor->empresa}}</td>
				            <td  class="center-align">{{$proveedor->direccion}}</td>
				            <td  class="center-align">{{$proveedor->telefono}}</td>
				             <td  class="center-align">{{$proveedor->celular}}</td>
				             <td  class="center-align">{{$proveedor->email}}</td>
				            <td  class="center-align">{{$proveedor->created_at}}</td>
				            <td  class="center-align">{{$proveedor->updated_at}}</td>
				            <td  class="center-align">
							{!! link_to_route('proveedores.edit', $title = 'Editar', $parameters = $proveedor->id, $attributes = array('class'=>'btn ')) !!}</td>
				            <td  class="center-align">{!! link_to_route('proveedores.edit', $title = 'Borrar', $parameters = $proveedor->id, $attributes = array('class'=>'btn red darken-3')) !!}</td>
			          	</tr>
					@endforeach
			    </tbody>
	      </table>
      </div>
    </div>
    <div class="col s12 center-align">
        <h6 class="bold">Cantidad registros: {!! $proveedores->total() !!}</h6>
    	{!! $proveedores->render() !!}
    </div>
@stop
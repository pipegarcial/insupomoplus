@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Editar Cat Referencia</h5>
        </div>

        <div class="col s12 center-align">
            {!! Form::model($categoriaReferencia,array('route' => ['categoria-referencia.update',$categoriaReferencia->id],'method'=>'PUT','class'=>'col s12')) !!}

                @include('categoria_referencias.include_form-categoria_referencia',['titleBtn' => 'Modificar']);

                {!! Form::close() !!}

    	</div>

        <div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso center-align">
            @if(Session::has('message'))
                <h6>Elemento categoría referencia editado </h6>       
            @endif      
        </div>
    </div>
@stop
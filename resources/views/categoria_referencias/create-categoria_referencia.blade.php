@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear Elemento Categoría Referencia</h5>
        </div>

        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'categoria-referencia.store','method' => 'POST','class'=>'col s12')) !!}

                @include('categoria_referencias.include_form-categoria_referencia',['titleBtn' => 'Crear']);

                {!! Form::close() !!}

    	</div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Elemento categoría referencia creado']);
    </div>
@stop
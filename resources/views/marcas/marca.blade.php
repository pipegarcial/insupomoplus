@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Marcas</h5>
        </div>


        <div class="s12 center-align">
           {!! link_to('marcas/create', $title = 'Agregar Marca', $parameters = array("class" => "btn margin-btn-form"), 
       $attributes = array()) !!}
        </div>
    </div>
     <div class="row">
    	@if(Session::has('message'))
     		<div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso">
               {{Session::get('message')}}           
        	</div>
        @endif 
	
		<div class="col s12 center-align">
           <h6 class="bold">Cantidad registros: {!! $marcas->total() !!}</h6>
    		{!! $marcas->render() !!}
        </div>
     
		
		<div class="col s12 ">
	        <table>
		        <thead >
		          	<tr>
			            <th data-field="id"  class="center-align">Código</th>
			            <th data-field="name"  class="center-align">Nombre</th>
			            <th data-field="price"  class="center-align">Fecha creación</th>
			            <th data-field="price"  class="center-align">Fecha Modificación</th>
			            <th data-field="price"  class="center-align">Modificar</th>
			            <th data-field="price"  class="center-align">Eliminar</th>
		          	</tr>
		        </thead>
			    <tbody>
			    	@foreach ($marcas as $marca)
			          	<tr  class="center-align">
				            <td  class="center-align">{{$marca->id}}</td>
				            <td  class="center-align">{{$marca->nombre}}</td>
				            <td  class="center-align">{{$marca->created_at}}</td>
				            <td  class="center-align">{{$marca->updated_at}}</td>
				            <td  class="center-align">
							{!! link_to_route('marcas.edit', $title = 'Editar', $parameters = $marca->id, $attributes = array('class'=>'btn ')) !!}</td>
				            <td  class="center-align">{!! link_to_route('marcas.edit', $title = 'Borrar', $parameters = $marca->id, $attributes = array('class'=>'btn red darken-3')) !!}</td>
			          	</tr>
					@endforeach
			    </tbody>
	      </table>
      </div>
    </div>
    <div class="col s12 center-align">
        <h6 class="bold">Cantidad registros: {!! $marcas->total() !!}</h6>
    	{!! $marcas->render() !!}
    </div>
@stop
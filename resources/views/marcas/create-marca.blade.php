@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear marca</h5>
        </div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Marca creada'])

        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'marcas.store','method' => 'POST','class'=>'col s12')) !!}

                @include('marcas.include_form-marca',['titleBtn' => 'Crear']);

                {!! Form::close() !!}

    	</div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Marca creada'])
    </div>
@stop
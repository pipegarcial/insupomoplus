@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Editar marca</h5>
        </div>

        <div class="col s12 center-align">
            {!! Form::model($marca,array('route' => ['marcas.update',$marca->id],'method'=>'PUT','class'=>'col s12')) !!}

                @include('marcas.include_form-marca',['titleBtn' => 'Modificar']);

                {!! Form::close() !!}

    	</div>

        <div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso center-align">
            @if(Session::has('message'))
                <h6>Material editado </h6>       
            @endif      
        </div>
    </div>
@stop
@extends('admin.admin')

@section('contentAdmin')
    <!-- Dropdown Structure -->
    <div class="row">

        <div class="col s12 left-align">
            <h5 class="important regular">Crear nuevo usuario</h5>
        </div>

        <div class="col s12 center-align">
            {!! Form::open(array('route' => 'user.store','method' => 'POST','class'=>'col s12')) !!}
                @include('admin.include_form-user',['titleBtn' => 'Registrar']);
            {!! Form::close() !!}


        </div>


   
         <div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso">
            @if(Session::get('message') == 'store')
                <h6>Usuario creado </h6>       
            @endif      
        </div>

    </div>
	
@stop
@extends('admin.admin')

@section('contentAdmin')
    <!-- Dropdown Structure -->
    <div class="row">

    	@if(Session::has('message'))
     		<div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso">
               {{Session::get('message')}}           
        	</div>
        @endif 
        <div class="col s12 left-align">
            <h5 class="important regular">Ver Usuarios</h5>
        </div>
		
		<div class="col s12 left-align">
	        <table>
		        <thead >
		          	<tr>
			            <th data-field="id"  class="center-align">Nombre</th>
			            <th data-field="name"  class="center-align">Apellido</th>
			            <th data-field="price"  class="center-align">Usuario</th>
			            <th data-field="price"  class="center-align">Modificar</th>
			            <th data-field="price"  class="center-align">Eliminar</th>
		          	</tr>
		        </thead>
			    <tbody>
			    	@foreach ($usuarios as $usuario)
			          	<tr  class="center-align">
				            <td  class="center-align">{{$usuario->usuario}}</td>
				            <td  class="center-align">{{$usuario->nombre}}</td>
				            <td  class="center-align">{{$usuario->apellido}}</td>
				            <td  class="center-align">
							{!! link_to_route('user.edit', $title = 'Editar', $parameters = $usuario->id, $attributes = array()) !!}

				            <i class="material-icons">mode_edit</i></td>
				            <td  class="center-align"><i class="material-icons">delete</i></td>
			          	</tr>
					@endforeach
			    </tbody>
	      </table>
      </div>
    </div>
	
@stop
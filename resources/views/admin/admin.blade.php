@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align border-bottom title-wrrapper">
            <h5 class="important bold">Administrador</h5>
        </div>
        <div class="col s3 center-align admin-btn-wrapper">
        	<ul class="admin-ul-buttons">
            <li> {!! link_to('user', $title = 'Ver usuarios', $parameters = array('class'=>'waves-effect waves-light btn'), $attributes = array()) !!}</li>
        		<li> {!! link_to('user/create', $title = 'Crear usuario', $parameters = array('class'=>'waves-effect waves-light btn'), $attributes = array()) !!}</li>
        	</ul>	
      	</div>

      <div class="col s9 border-left">
			   @yield('contentAdmin')
      </div>
    </div>
	
@stop
@extends('admin.admin')

@section('contentAdmin')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 left-align">
            <h5 class="important regular">Editar usuario</h5>
        </div>

        <div class="col s12 center-align">
            {!! Form::model($user,array('route' => ['user.update',$user->id],'method'=>'PUT','class'=>'col s12')) !!}

                @include('admin.include_form-user',['titleBtn' => 'Modificar']);

                {!! Form::close() !!}

    	</div>


   
        <div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso">
            @if(Session::has('message'))
                <h6>Usuario modificado </h6>       
            @endif      
        </div>
    </div>
@stop
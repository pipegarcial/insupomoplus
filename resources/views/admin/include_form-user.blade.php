<div class="col s12 m12 l6">
    <div class="input-field col s12">
        <i class="material-icons prefix">assignment_ind</i>
        {!! Form::text('nombre',null, array('class' => 'validate','id' => 'icon_prefix')) !!}
        {!! Form::label('icon_prefix', 'Nombre...')  !!}

        {!! $errors->first('nombre', "<span class='error'>:message</span>") !!}
    </div>

                    <div class="input-field col s12">
        <i class="material-icons prefix">assignment_ind</i>
        {!! Form::text('apellido',null, array('class' => 'validate','id' => 'icon_lastname')) !!}
        {!! Form::label('icon_lastname', 'Apellido...')  !!}

        {!! $errors->first('apellido', "<span class='error'>:message</span>") !!}
    </div>
     
    <div class="input-field col s12">
        <i class="material-icons prefix">account_circle</i>
            {!! Form::text('usuario',null, array('class' => 'validate','id' => 'icon_username')) !!}
        {!! Form::label('icon_username', 'Nombre usuario...')  !!}

        {!! $errors->first('usuario', "<span class='error'>:message</span>") !!}
    </div>
</div>

<div class="col s6">
        <div class="input-field col s12">
        <i class="material-icons prefix">lock</i>
        {!! Form::password('password', array('class' => 'validate','id' => 'icon_password')) !!}
        {!! Form::label('icon_password', 'Contraseña...')  !!}

        {!! $errors->first('password', "<span class='error'>:message</span>") !!}
    </div>
    <div class="input-field col s12">
        <i class="material-icons prefix">lock</i>
        {!! Form::password('password_confirmation', array('class' => 'validate','id' => 'icon_password_repeat')) !!}
        {!! Form::label('icon_password_repeat', 'Repetir Contraseña...')  !!}

        {!! $errors->first('password_confirmation', "<span class='error'>:message</span>") !!}
    </div>
    <div class="col s12 m12 l12 right-align">
        {!! Form::submit($titleBtn,array("class" => "btn margin-btn-form")) !!}
    </div>
</div>
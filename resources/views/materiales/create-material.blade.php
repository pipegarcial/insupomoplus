@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear Material</h5>
        </div>
      @include('includes.include_mensaje-creado',['mensaje' => 'Material creado'])
        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'materiales.store','method' => 'POST','class'=>'col s12')) !!}

                @include('materiales.include_form-material',['titleBtn' => 'Crear'])
                

                {!! Form::close() !!}

    	</div>

       @include('includes.include_mensaje-creado',['mensaje' => 'Material creado'])
    </div>
@stop
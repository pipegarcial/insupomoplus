@extends('layouts.main')

@section('content')

	<div class="col s12 m12 l12 center-align">
		<h4>Iniciar sesión</h4>
	</div>

    
    <div class="col s12 m12 l12 center-align error">
        @if(Session::has('message-error'))
        {{Session::get('message-error')}}
    @endif
            </div>

	<div class="col s12 m12 offset-l3 l6 center-align">
		  {!! Form::open(array('route' => 'login.store','method' => 'POST','class'=>'col s12')) !!}
	        <div class="input-field col s12">
	            <i class="material-icons prefix">account_circle</i>
	            {!! Form::text('usuario',null, array('class' => 'validate','id' => 'icon_prefix')) !!}
	            {!! Form::label('icon_prefix', 'usuario..')  !!}

	            {!! $errors->first('usuario', "<span class='error'>:message</span>") !!}
	        </div>

        	<div class="input-field col s12">
            	<i class="material-icons prefix">account_circle</i>
                {!! Form::password('password', array('class' => 'validate','id' => 'icon_password')) !!}
               	{!! Form::label('icon_password', 'Contraseña..')  !!}

                {!! $errors->first('password', "<span class='error'>:message</span>") !!}
        	</div>

            <div class="input-field col s12"> 
               {!! Form::checkbox('remember',null,'true',array('class' => 'filled-in','id' => 'filled-in-box')) !!}

                <label for="filled-in-box">Recuerdame</label>
            </div>

            <div class="col s12 m12 l12 center-align">
                {!! Form::submit('Login',array("class" => "btn margin-btn-form")) !!}
            </div>


   
        {!! Form::close() !!}
    </div>

	<div class="col s12 m12 l12 center-align">
	</div>
@stop
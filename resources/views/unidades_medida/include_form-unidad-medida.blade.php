<div class="col s12 m12 l6 offset-l3 center-align">
    <div class="input-field col s12 center-align">
        <i class="material-icons prefix">assignment_ind</i>
        {!! Form::text('valor',null, array('class' => 'validate','id' => 'icon_prefix')) !!}
        {!! Form::label('icon_prefix', 'Valor unidad de medida')  !!}

        {!! $errors->first('valor', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="col s12 m12 l12 center-align">
        {!! Form::submit($titleBtn,array("class" => "btn margin-btn-form btn-40")) !!}
    </div>

    <div class="col s12 m12 l12 center-align">
       {!! link_to('unidades-medida', $title = 'Volver', $parameters = array("class" => "btn margin-btn-form btn-40"), 
       $attributes = array()) !!}
    </div>
</div>
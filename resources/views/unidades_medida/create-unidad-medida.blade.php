@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear unidad de medida</h5>
        </div>
        
        @include('includes.include_mensaje-creado',['mensaje' => 'Marca creada']);
        
        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'unidades-medida.store','method' => 'POST','class'=>'col s12')) !!}

                @include('unidades_medida.include_form-unidad-medida',['titleBtn' => 'Crear']);

                {!! Form::close() !!}

    	</div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Marca creada']);
    </div>
@stop
@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Editar unidad de medida</h5>
        </div>

        <div class="col s12 center-align">
            {!! Form::model($unidadMedida,array('route' => ['unidades-medida.update',$unidadMedida->id],'method'=>'PUT','class'=>'col s12')) !!}

                @include('unidades_medida.include_form-unidad-medida',['titleBtn' => 'Modificar']);

                {!! Form::close() !!}

    	</div>

        <div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso center-align">
            @if(Session::has('message'))
                <h6>Unidad de medida editada </h6>       
            @endif      
        </div>
    </div>
@stop
@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear color</h5>
        </div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Color creado'])

        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'colores.store','method' => 'POST','class'=>'col s12')) !!}

                @include('colores.include_form-color',['titleBtn' => 'Crear'])

                {!! Form::close() !!}

    	</div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Color creado'])
    </div>
@stop
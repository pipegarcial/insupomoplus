@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">colores</h5>
        </div>


        <div class="s12 center-align">
           {!! link_to('colores/create', $title = 'Agregar color', $parameters = array("class" => "btn margin-btn-form"), 
       $attributes = array()) !!}
        </div>
    </div>
     <div class="row">
    	@if(Session::has('message'))
     		<div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso">
               {{Session::get('message')}}           
        	</div>
        @endif 
	
		<div class="col s12 center-align">
           <h6 class="bold">Cantidad registros: {!! $colores->total() !!}</h6>
    		{!! $colores->render() !!}
        </div>
     
		
		<div class="col s12 ">
	        <table>
		        <thead >
		          	<tr>
			            <th data-field="id"  class="center-align">Código</th>
			            <th data-field="name"  class="center-align">Nombre</th>
			            <th data-field="price"  class="center-align">Fecha creación</th>
			            <th data-field="price"  class="center-align">Fecha Modificación</th>
			            <th data-field="price"  class="center-align">Modificar</th>
			            <th data-field="price"  class="center-align">Eliminar</th>
		          	</tr>
		        </thead>
			    <tbody>
			    	@foreach ($colores as $color)
			          	<tr  class="center-align">
				            <td  class="center-align">{{$color->id}}</td>
				            <td  class="center-align">{{$color->nombre}}</td>
				            <td  class="center-align">{{$color->created_at}}</td>
				            <td  class="center-align">{{$color->updated_at}}</td>
				            <td  class="center-align">
												{!! link_to_route('colores.edit', $title = 'Editar', $parameters = $color->id, $attributes = array('class'=>'btn ')) !!}</td>
				  <td  class="center-align">{!! link_to_route('colores.edit', $title = 'Borrar', $parameters = $color->id, $attributes = array('class'=>'btn red darken-3')) !!}</td>
			          	</tr>
					@endforeach
			    </tbody>
	      </table>
      </div>
    </div>
    <div class="col s12 center-align">
        <h6 class="bold">Cantidad registros: {!! $colores->total() !!}</h6>
    	{!! $colores->render() !!}
    </div>
@stop
<!-- ================================================== -->
<!-- Macro create for use collection-->
{!! Form::macro( 'DBSelect', function($fieldName, $collection, $options=array('id' => 'empresa')){ $key = key($options); $rows = $collection->lists($options[$key], $key ); return Form::select($fieldName,$rows,null,array('class' => 'validate','id' => 'icon_country'),array('multiple'));
}) !!}
<!-- ================================================== -->
<div class="col s12 m12 l6 offset-l3 center-align">
  <div class="input-field col s12 center-align">
    {!! Form::text('nombre',null, array('class' => 'validate','id' => 'nombre')) !!} {!! Form::label('nombre', 'Nombre material...') !!} {!! $errors->first('nombre', "<span class='error center-align'>:message</span>") !!}
  </div>
  <!-- ================================================== -->
  <div class="input-field col s12 center-align">
    {!! Form::DBSelect('categoria_insumo_id',$categoriaInsumo,array('id' => 'nombre'),'categoria-id') !!} {!! Form::label('categoria_insumo_id', 'Categoría Insumo...') !!} {!! $errors->first('categoria_insumo_id', "<span class='error'>:message</span>")
    !!}
  </div>
  <!-- ================================================== -->
  <div class="input-field col s12 center-align">
    {!! Form::text('precio',null, array('class' => 'validate','id' => 'precio')) !!} {!! Form::label('precio', 'Precio...') !!} {!! $errors->first('precio', "<span class='error center-align'>:message</span>") !!}
  </div>
  <!-- ================================================== -->
  <!-- Proveedores -->
  <div class="input-field col s7">
    <select id="select-prov-ins" multiple name="proveedores-list[]" class='custom-scroll'>
      <option value="" disabled selected>Escoge el o los proveedores</option>
      @foreach($proveedores as $proveedor)
      <option value='{{$proveedor->id}}'>{{$proveedor->empresa}}</option>
      @endforeach
    </select>
    {!! $errors->first('proveedores-list[]', "<span class='error'>:message</span>") !!}
  </div>
  <div class="input-field col s5">
    <input id="serch-prov-insumos" type="text" class="validate search-dina" data-nombre="insumos" data-id-select="select-prov-ins">
    <label for="serch-prov-insumos">Buscar proveedors</label>
  </div>
  <!-- ================================================== -->
  <div class="col s12 m12 l6 center-align">
    {!! link_to('insumos', $title = 'Volver', $parameters = array("class" => "btn margin-btn-form btn-40"), $attributes = array()) !!}
  </div>
  <!-- ================================================== -->
  <div class="col s12 m12 l6 center-align">
    {!! Form::submit($titleBtn,array("class" => "btn margin-btn-form btn-40")) !!}
  </div>
</div>
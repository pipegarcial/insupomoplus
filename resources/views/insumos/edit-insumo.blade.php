@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Editar Insumo</h5>
        </div>

        <div class="col s12 center-align">
            {!! Form::model($insumos,array('route' => ['insumos.update',$insumos->id],'method'=>'PUT','class'=>'col s12')) !!}

                @include('insumos.include_form-insumo',['titleBtn' => 'Modificar'])

            {!! Form::close() !!}
    	</div>
    </div>
    
    <!-- ===================================== -->
    <div class="col s12">
        <hr class="orange-line" />
    </div>

    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Lista Proveedores</h5>
        </div>
        <div class="col s12 center-align">
            <table>
                <thead >
                    <tr>
                        <th data-field="id"  class="center-align">Código</th>
                        <th data-field="name"  class="center-align">Empresa</th>
                        <th data-field="price"  class="center-align">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($proveedoresList as $proveedoList)
                        <tr  class="center-align">
                            <td  class="center-align">{{$proveedoList->id}}</td>
                            <td  class="center-align">{{$proveedoList->empresa}}</td>
                            <td  class="center-align">{!! link_to_route('proveedores.edit', $title = 'Borrar', $parameters = $proveedoList->id, $attributes = array('class'=>'btn red darken-3')) !!}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    
        <div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso center-align">
            @if(Session::has('message'))
                <h6>Material editado </h6>       
            @endif      
        </div>
    </div>
@stop
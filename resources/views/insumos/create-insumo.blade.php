@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear Insumo</h5>
        </div>
      @include('includes.include_mensaje-creado',['mensaje' => 'Insumo creado'])
        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'insumos.store','method' => 'POST','class'=>'col s12')) !!}

                @include('insumos.include_form-insumo',['titleBtn' => 'Crear'])
                

                {!! Form::close() !!}

    	</div>

       @include('includes.include_mensaje-creado',['mensaje' => 'Insumo creado'])
    </div>
@stop
@extends('layouts.main_app') @section('content')
<!-- Dropdown Structure -->
<div class="row">
	<div class="col s12 center-align">
		<h5 class="important regular">Editar Insupro</h5>
	</div>
	<!-- ================================================== -->
	<!-- Macro create for use collection from model Country -->
	{!! Form::macro( 'DBSelect', function($fieldName,$collection,$options=array('code' => 'name'), $id){ $key = key($options); $rows = $collection->lists($options[$key], $key ); return Form::select($fieldName,$rows,null,array('class' => 'validate','id' =>
	$id)); }) !!} @include('includes.include_mensaje-creado',['mensaje' => 'Insupro editado'])
	<div class="col s12 center-align">
		{!! Form::model($insupro,array('route' => ['insupros.update',$insupro->id],'method'=>'PUT','class'=>'col s12')) !!}


		<div class="col s12 m12 l12 center-align">
			<div class="col s12">
				<hr class="orange-line" />
			</div>
			<div class="row">
				<div class="col s12">
					<ul class="tabs">
						<li class="tab col s3">
							<a class="active" href="#test1">Basico</a>
						</li>
						<li class="tab col s3">
							<a href="#test2">Piezas</a>
						</li>
						<li class="tab col s3">
							<a href="#test3">Insumos</a>
						</li>
						<li class="tab col s3">
							<a href="#test4">Confecciones</a>
						</li>
						<li class="tab col s3">
							<a href="#test5">Oficios Varios</a>
						</li>
					</ul>
				</div>
				<div id="test1" class="col s12">

					<div class="input-field col s12 m12 center-align">
						{!! Form::text('nombre',null, array('class' => 'validate','id' => 'nombre-insupro')) !!} {!! Form::label('nombre-insupro', 'Nombre...') !!} {!! $errors->first('nombre_basico_insupro', "<span class='error center-align'>:message</span>") !!}
					</div>

					<!-- ===================================== -->
					<div class="input-field col s6 center-align">
						{!! Form::DBSelect('referencia',$referencias,array('id' => 'nombre'),'referencia-id') !!} {!! Form::label('categoria-id', 'Referencia...') !!} {!! $errors->first('categoria-id', "<span class='error'>:message</span>") !!}
					</div>
					<div class="input-field col s6">
						<input id="serch-referencia" type="text" class="validate search" data-id-select="referencia-id">
						<label for="serch-referencia">Buscar en cat referencia</label>
					</div>
					<!-- ===================================== -->
				</div>
				<div id="test2" class="col s12">
					<!-- PIEZAS -->
					<!-- ===================================== -->
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<div id="piezas-insupro-contenedor" class="col s12">
						<div class="input-field col s12 m6 center-align">
							{!! Form::text('nombre_pieza',null, array('class' => 'validate','id' => 'nombre-insupro-pieza')) !!} {!! Form::label('nombre-insupro-pieza', 'Nombre...') !!} {!! $errors->first('nombre_pieza', "<span class='error center-align'>:message</span>")
							!!}
							<span id="error-pieza-nombre" class='error'></span>
						</div>
						<!-- ================================================== -->
						<!-- Material -->
						<div class="col s12 m6">
							<div class="input-field col s12 m7 center-align">
								<select id="material-insupro-piezas" name="material_id">
									<option value="" disabled selected>Seleccionar...</option>
									@foreach($materiales as $material)
									<option value='{{$material->id}}'>{{$material->nombre}}</option>
									@endforeach
								</select>
								<label for="label-material">Material</label>
							</div>
							<div class="input-field col s12 m5">
								<input id="serch-material-insupro-piezas" type="text" class="validate search" data-id-select="material-insupro-piezas">
								<label for="serch-material">Buscar en material</label>
							</div>
							<span id="error-pieza-material" class='error'></span>
						</div>
						<!-- ================================================== -->
						<!-- Color -->
						<div class="col s12 m6">
							<div class="input-field col s12 m7 center-align">
								<select id="color-insupro-piezas" name="material_id" class=''>
									<option value="" disabled selected>Seleccionar...</option>
									@foreach($colores as $color)
									<option value='{{$color->id}}'>{{$color->nombre}}</option>
									@endforeach
								</select>
								<label for="first_name">Color</label>
							</div>
							<div class="input-field col s12 m5">
								<input id="serch-color-insupro-piezas" type="text" class="validate search" data-id-select="color-insupro-piezas">
								<label for="color-insupro-piezas">Buscar en material</label>
							</div>
						</div>
						<!-- ================================================== -->
						<div class="input-field col s12 m6  center-align">
							{!! Form::text('cantidad-pieza-name',null, array('class' => 'validate','id' => 'cantidad-pieza')) !!} {!! Form::label('cantidad-pieza', 'Cantidad...') !!} {!! $errors->first('cantidad-pieza-name', "<span class='error center-align'>:message</span>")
							!!}
							<span id="error-pieza-cantidad" class='error'></span>
						</div>
						<!-- ================================================== -->
						<div class="input-field col s12 m6  center-align">
							{!! Form::text('ancho-pieza-name',null, array('class' => 'validate col s12','id' => 'ancho-pieza')) !!} {!! Form::label('ancho-pieza', 'Ancho...') !!} {!! $errors->first('ancho-pieza-name', "<span class='error center-align'>:message</span>") !!}
						</div>
						<!-- ================================================== -->
						<div class="input-field col s12 m6  center-align">
							{!! Form::text('largo-pieza-name',null, array('class' => 'validate col s12','id' => 'largo-pieza')) !!} {!! Form::label('largo-pieza', 'Largo...') !!} {!! $errors->first('largo', "<span class='error center-align'>:message</span>") !!}
						</div>
						<!-- =================  Mensaje éxito incrustado por ajax en app.js ====================== -->
						<div class="exito-pieza col offset-s4 s4  center-align card-panel  light-green darken-1 exitoso center-align">
							<h6></h6>
						</div>
						<!-- =========================================================================== -->
						<div class="col s12 center-align">
							<button id="agregar-pieza" type="button" class="waves-effect waves-light btn btn-agregar-insupros" data-insupro="{{$insupro->id}}">Agregar pieza</button>
						</div>
						<div class="col s12 margin-normal">
							<div class="divider"></div>
						</div>
						<div id="resultados-piezas-agregadas" class="col s12 margin-normal">
							<table>
								<thead>
									<tr>
										<th data-field="name" class="center-align">Nombre</th>
										<th data-field="name" class="center-align">Material</th>
										<th data-field="price" class="center-align">Color</th>
										<th data-field="price" class="center-align">Cantidad</th>
										<th data-field="price" class="center-align">Ancho</th>
										<th data-field="price" class="center-align">Largo</th>
										<th data-field="price" class="center-align">Eliminar</th>
									</tr>
								</thead>
								<tbody id="piezas-tbody">
									@foreach ($piezas as $pieza)
									<tr id="pieza-{{$pieza->id}}" class="center-align">
										<td class="center-align">{{$pieza->nombre}}</td>
										<td class="center-align">{{$pieza->material_id}}</td>
										<td class="center-align">{{$pieza->color_id}}</td>
										<td class="center-align">{{$pieza->cantidad}}</td>
										<td class="center-align">{{$pieza->ancho}}</td>
										<td class="center-align">{{$pieza->largo}}</td>
										</td>
										<td class='center-align'>
											<button type='button' class='waves-effect waves-light btn red darken-4' data-id-insu='{{$pieza->id}}'>
												<i class='material-icons'>cancel</i></button>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<!-- ======================================= -->
					</div>
				</div>
			</div>
			<!-- ===================================== -->
			<!-- ISUMOS -->
			<!-- ===================================== -->
			<div id="test3" class="col s12">
				<div id="insumos-insupro-contenedor" class="col s12">
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token-insumos">
					<!-- ======================================= -->
					<div class="input-field col s6 center-align">
						{!! Form::text('cantidad-insumo',null, array('class' => 'validate','id' => 'cantidad-insumo')) !!} {!! Form::label('cantidad-insumo', 'Cantidad...') !!} {!! $errors->first('cantidad-insumo', "<span class='error center-align'>:message</span>") !!}
					</div>
					<!-- ======================================= -->
					<div class="col s6">
						<div class="input-field col s7 center-align">
							{!! Form::DBSelect('insumo',$insumos,array('id' => 'nombre'),'insumo-insupro-id') !!} {!! Form::label('categoria-id', 'Insumos...') !!} {!! $errors->first('categoria-id', "<span class='error'>:message</span>") !!}
						</div>
						<div class="input-field col s5">
							<input id="serch-insu" type="text" class="validate search" data-id-select="insumo-insupro-id">
							<label for="serch-insu">Buscar insumo...</label>
						</div>
					</div>
					<!-- ======================================= -->
					<div class="input-field col s12 m6 center-align">
						{!! Form::text('consumo-insumo',null, array('class' => 'validate','id' => 'consumo-insumo')) !!} {!! Form::label('consumo-insumo', 'Consumo...') !!} {!! $errors->first('consumo-insumo', "<span class='error center-align'>:message</span>") !!}
					</div>
					<!-- ======================================= -->
					<div class="input-field col s12 m6 center-align">
						{!! Form::text('obser-insumo',null, array('class' => 'materialize-textarea','id' => 'obser-insumo-textarea','length'=>'120')) !!} {!! Form::label('obser-insumo-textarea', 'Observaciones...') !!} {!! $errors->first('nombre', "<span class='error center-align'>:message</span>")
						!!}
					</div>
					<!-- ======================================= -->
					<div class="col s12 center-align">
						<button id="agregar-insumo-insupro" type="button" class="waves-effect waves-light btn btn-agregar-insupros" data-insupro="{{$insupro->id}}">Agregar insumo</button>
					</div>
					<!-- ======================================= -->
					<!-- ======================================= -->
					<div class="exito-insumo-insupro col offset-s4 s4  center-align card-panel  light-green darken-1 exitoso center-align">
						<h6>El insumo se a agregado se ha creado</h6>
					</div>
					<div class="col s12 margin-normal">
						<div class="divider"></div>
					</div>
					<div id="resultados-insumos-agregadas" class="col s12 margin-normal">
						<table>
							<thead>
								<tr>
									<th data-field="name" class="center-align">Insumo</th>
									<th data-field="name" class="center-align">Cantidad</th>
									<th data-field="price" class="center-align">Consumo</th>
									<th data-field="price" class="center-align">Observaciones</th>
									<th data-field="price" class="center-align">Eliminar</th>
								</tr>
							</thead>
							<tbody id="insumos-tbody">
								@foreach ($insumos_insupros as $insuInsupro)
								<tr id="insumo-insupro-{{$insuInsupro->id}}" class="center-align">
									<td class="center-align">{{$insuInsupro->insumo_id}}</td>
									<td class="center-align">{{$insuInsupro->cantidad}}</td>
									<td class="center-align">{{$insuInsupro->consumo}}</td>
									<td class="center-align">{{$insuInsupro->observaciones}}</td>
									<td class='center-align'>
										<button type='button' class='waves-effect waves-light btn red darken-4' data-id-insumo-insupro='{{$insuInsupro->id}}'>
											<i class='material-icons'>cancel</i></button>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- ===================================== -->
			<!-- CONFECCIONES -->
			<!-- ===================================== -->
			<div id="test4" class="col s12">
				<div id="confecciones-insupro-contenedor" class="col s12">
					<!-- ======================================= -->
					<div class="input-field col s6 center-align">
						{!! Form::text('proceso-confecciones',null, array('class' => 'validate','id' => 'proceso-confecciones')) !!} {!! Form::label('proceso-confecciones', 'Proceso...') !!} {!! $errors->first('proceso-confecciones', "<span class='error center-align'>:message</span>")
						!!}
					</div>
					<!-- ======================================= -->

					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token-confecciones">

					<div class="col s6">
						<div class="input-field col s7 center-align">
							{!! Form::DBSelect('maquina',$maquinas,array('id' => 'nombre'),'maquina-id') !!} {!! Form::label('categoria-id', 'Maquina...') !!}
						</div>
						<div class="input-field col s5">
							<input id="serch-maquina" type="text" class="validate search" data-id-select="maquina-id">
							<label for="serch-maquina">Buscar maquina...</label>
						</div>
					</div>
					<!-- ======================================= -->
					<!-- ======================================= -->
					<div class="input-field col s12 m6 center-align">
						{!! Form::text('recorrido_confecciones',null, array('class' => 'validate','id' => 'recorrido-confecciones')) !!} {!! Form::label('recorrido-confecciones', 'Recorrido...') !!} {!! $errors->first('recorrido_confecciones', "<span class='error center-align'>:message</span>")
						!!}
					</div>
					<!-- ======================================= -->
					<div class="input-field col s12 m6 center-align">
						{!! Form::text('cantidad_confecciones',null, array('class' => 'validate','id' => 'cantidad-confecciones')) !!} {!! Form::label('cantidad-confecciones', 'Cantidad...') !!} {!! $errors->first('cantidad_confecciones', "<span class='error center-align'>:message</span>")
						!!}
					</div>
					<!-- ======================================= -->
					<div class="exito-confeccion col offset-s4 s4  center-align card-panel  light-green darken-1 exitoso center-align">
						<h6></h6>
					</div>

					<div class="col s12 center-align">
						<button id="agregar-confeccion" type="button" class="waves-effect waves-light btn btn-agregar-insupros" data-insupro="{{$insupro->id}}">Agregar Confecciones</button>
					</div>

					<div class="col s12 margin-normal">
						<div class="divider"></div>
					</div>
					<div id="resultados-confecciones-agregadas" class="col s12 margin-normal">
						<table>
							<thead>
								<tr>
									<th data-field="name" class="center-align">Proceso</th>
									<th data-field="name" class="center-align">Maquina</th>
									<th data-field="name" class="center-align">Recorrido</th>
									<th data-field="price" class="center-align">Cantidad</th>
									<th data-field="price" class="center-align">Eliminar</th>
								</tr>
							</thead>
							<tbody id="confeccion-tbody">
								@foreach ($confecciones as $confeccion)
								<tr id="oficio-insupro-{{$confeccion->id}}" class="center-align">
									<td class="center-align">{{$confeccion->proceso_confecciones}}</td>
									<td class="center-align">{{$confeccion->maquina_id}}</td>
									<td class="center-align">{{$confeccion->recorrido}}</td>
									<td class="center-align">{{$confeccion->cantidad}}</td>
									<td class='center-align'>
										<button type='button' class='waves-effect waves-light btn red darken-4' data-id-insu-insupro='{{$pieza->id}}'>
											<i class='material-icons'>cancel</i></button>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<!-- ======================================= -->
				</div>
			</div>
			<!-- ===================================== -->
			<!-- OFICIOS VARIOS -->
			<!-- ===================================== -->
			<div id="test5" class="col s12">
				<div id="oficios-insupro-contenedor" class="col s12">
					<!-- ======================================= -->
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token-oficios">
					<div class="input-field col s12 m6 center-align">
						{!! Form::text('proceso_oficio',null, array('class' => 'validate','id' => 'proceso-oficio')) !!} {!! Form::label('proceso-oficio', 'Proceso...') !!} {!! $errors->first('proceso_oficio', "<span class='error center-align'>:message</span>") !!}
					</div>
					<!-- ======================================= -->

					<div class="col s6">
						<div class="input-field col s7 center-align">
							{!! Form::DBSelect('oficios',$oficios,array('id' => 'nombre'),'oficios-id') !!} {!! Form::label('categoria-id', 'Oficios...') !!}
						</div>
						<div class="input-field col s5">
							<input id="serch-oficios" type="text" class="validate search" data-id-select="oficios-id">
							<label for="serch-oficios">Buscar maquina...</label>
						</div>
					</div>
					<!-- ======================================= -->
					<!-- ======================================= -->
					<div class="input-field col s12 m6 center-align">
						{!! Form::text('cantidad_oficio',null, array('class' => 'validate','id' => 'cantidad-oficio')) !!} {!! Form::label('cantidad-oficio', 'Cantidad...') !!} {!! $errors->first('cantidad_oficio', "<span class='error center-align'>:message</span>") !!}
					</div>

					<!-- ======================================= -->
					<div class="exito-oficio col offset-s4 s4  center-align card-panel  light-green darken-1 exitoso center-align">
						<h6></h6>
					</div>
					<div class="col s12 center-align">
						<button id="agregar-oficio-insupro" type="button" class="waves-effect waves-light btn btn-agregar-insupros" data-insupro="{{$insupro->id}}">Agregar Oficio</button>
					</div>
					<div class="col s12 margin-normal">
						<div class="divider"></div>
					</div>
					<div id="resultados-piezas-agregadas" class="col s12 margin-normal">
						<table>
							<thead>
								<tr>
									<th data-field="name" class="center-align">Proceso</th>
									<th data-field="name" class="center-align">Oficios</th>
									<th data-field="price" class="center-align">Cantidad</th>
									<th data-field="price" class="center-align">Eliminar</th>
								</tr>
							</thead>
							<tbody id="oficio-tbody">
								@foreach ($oficiosInsupro as $oficioInsu)
								<tr id="oficio-insupro-{{$oficioInsu->id}}" class="center-align">
									<td class="center-align">{{$oficioInsu->proceso_oficio}}</td>
									<td class="center-align">{{$oficioInsu->oficio_id}}</td>
									<td class="center-align">{{$oficioInsu->cantidad}}</td>
									<td class='center-align'>
										<button type='button' class='waves-effect waves-light btn red darken-4' data-id-insu-insupro='{{$pieza->id}}'>
											<i class='material-icons'>cancel</i></button>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<!-- ======================================= -->
				</div>
			</div>
		</div>
		<div class="col s12">
			<hr class="orange-line" />
		</div>
		<!-- ===================================== -->
		<!-- ===================================== -->

		<div class="col s12 m12 l6 center-align">
			{!! link_to('insupros', $title = 'Volver', $parameters = array("class" => "btn margin-btn-form btn-40 col s12"), $attributes = array()) !!}
		</div>

		<div class="col s12 m12 l6 center-align">
			{!! Form::submit('Editar',array("class" => "btn margin-btn-form btn-40 col s12")) !!}
		</div>
	</div>
	@include('includes.include_mensaje-creado',['mensaje' => 'Insupro editado']) {!! Form::close() !!}

</div>


</div>
@stop
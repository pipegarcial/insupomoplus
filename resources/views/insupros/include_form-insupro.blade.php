<!-- ================================================== -->
<!-- Macro create for use collection from model Country -->
{!! Form::macro( 'DBSelect', function($fieldName,$collection,$options=array('code' => 'name'), $id){ $key = key($options); $rows = $collection->lists($options[$key], $key ); return Form::select($fieldName,$rows,null,array('class' => 'validate','id' =>
$id)); }) !!}
<div class="col s12 m12 l12 center-align">
  <div class="col s12">
    <hr class="orange-line" />
  </div>
  <div class="row">
    <div class="input-field col s12 center-align">
      {!! Form::text('nombre',null, array('class' => 'validate','id' => 'nombre-insupro')) !!} {!! Form::label('nombre-insupro', 'Nombre...') !!} {!! $errors->first('nombre_basico_insupro', "<span class='error center-align'>:message</span>") !!} {!! $errors->first('nombre',
      "<span class='error center-align'>:message</span>") !!}

    </div>

    <!-- ================================================== -->
    <!-- Cat Referencia -->
    <div class="input-field col s6 center-align">
      {!! Form::DBSelect('referencia',$referencias,array('id' => 'nombre'),'referencia-id') !!} {!! Form::label('categoria-id', 'Referencia...') !!} {!! $errors->first('categoria-id', "<span class='error'>:message</span>") !!}
    </div>
    <div class="input-field col s6">
      <input id="serch-referencia" type="text" class="validate search" data-id-select="referencia-id">
      <label for="serch-referencia">Buscar en cat referencia</label>
    </div>
    <!-- ===================================== -->


    <div class="col s12">
      <hr class="orange-line" />
    </div>
    <!-- ===================================== -->
    <!-- ===================================== -->
    <div class="col s12 m12 l6 center-align">
      {!! link_to('proveedores', $title = 'Volver', $parameters = array("class" => "btn margin-btn-form btn-40 col s12"), $attributes = array()) !!}
    </div>

    <div class="col s12 m12 l6 center-align">
      {!! Form::submit($titleBtn,array("class" => "btn margin-btn-form btn-40 col s12")) !!}
    </div>
  </div>
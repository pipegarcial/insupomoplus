@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear Insupro</h5>
        </div>

        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'insupros.store','method' => 'POST','class'=>'col s12')) !!}

                @include('insupros.include_form-insupro',['titleBtn' => 'Crear'])

                {!! Form::close() !!}

    	</div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Cliente creado'])
    </div>
@stop
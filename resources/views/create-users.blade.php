@extends('admin')

@section('contentAdmin')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 left-align">
            <h5 class="important regular">Crear usuario</h5>
        </div>

        <div class="col s12 center-align">
            {!! Form::open(array('url' => 'foo/bar')) !!}

                <div class="col s12 m12 l6">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">assignment_ind</i>
                        {!! Form::text('nombre',null, array('class' => 'validate','id' => 'icon_prefix')) !!}
                        {!! Form::label('icon_prefix', 'Nombre...')  !!}

                        {!! $errors->first('nombre', "<span class='error'>:message</span>") !!}
                    </div>

                    <div class="input-field col s12">
                        <i class="material-icons prefix">assignment_ind</i>
                        {!! Form::text('apellido',null, array('class' => 'validate','id' => 'icon_lastname')) !!}
                        {!! Form::label('apellido', 'Apellido...')  !!}

                        {!! $errors->first('email', "<span class='error'>:message</span>") !!}
                    </div>
     
                    <div class="input-field col s12">
                        <i class="material-icons prefix">account_circle</i>
                         {!! Form::text('usuario',null, array('class' => 'validate','id' => 'icon_username')) !!}
                        {!! Form::label('icon_username', 'Nombre usuario...')  !!}

                        {!! $errors->first('usuario', "<span class='error'>:message</span>") !!}
                    </div>
                </div>

                <div class="col s6">
    
                    <div class="input-field col s12">
                        <i class="material-icons prefix">lock</i>
                        {!! Form::password('contrasena', array('class' => 'validate','id' => 'icon_password')) !!}
                        {!! Form::label('icon_password', 'Contraseña...')  !!}

                        {!! $errors->first('contrasena', "<span class='error'>:message</span>") !!}
                    </div>

                    <<div class="input-field col s12">
                        <i class="material-icons prefix">lock</i>
                        {!! Form::password('contrasena', array('class' => 'validate','id' => 'icon_password_rep')) !!}
                        {!! Form::label('icon_password_rep', 'Contraseña...')  !!}

                        {!! $errors->first('contrasena', "<span class='error'>:message</span>") !!}
                    </div>


                    <div class="col s12 m12 l12 right-align">
                        {!! Form::submit('Registrar',array("class" => "btn margin-btn-form")) !!}
                    </div>
                </div>

                {!! Form::close() !!}
    </div>
	
@stop
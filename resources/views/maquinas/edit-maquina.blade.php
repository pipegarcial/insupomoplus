@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Editar Maquinas</h5>
        </div>
        
      @include('includes.include_mensaje-creado',['mensaje' => 'Maquina editada'])
        <div class="col s12 center-align">
            {!! Form::model($maquinas,array('route' => ['maquinas.update',$maquinas->id],'method'=>'PUT','class'=>'col s12')) !!}

                @include('maquinas.include_form-maquina',['titleBtn' => 'Modificar'])

            {!! Form::close() !!}

    	</div>
       @include('includes.include_mensaje-creado',['mensaje' => 'Maquina editada'])
    </div>
@stop
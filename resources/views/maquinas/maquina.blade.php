@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Maquinas</h5>
        </div>


        <div class="s12 center-align">
           {!! link_to('maquinas/create', $title = 'Agregar maquina', $parameters = array("class" => "btn margin-btn-form"), 
       		$attributes = array()) !!}
        </div>
    </div>
     <div class="row">
	
		<div class="col s12 center-align">
           <h6 class="bold">Cantidad registros: {!! $maquinas->total() !!}</h6>
    		{!! $maquinas->render() !!}
        </div>
     
		
		<div class="col s12 ">
	        <table>
		        <thead >
		          	<tr>
			            <th data-field="id"  class="center-align">Código</th>
			            <th data-field="name"  class="center-align">Nombre</th>
			            <th data-field="price"  class="center-align">Factor</th>
			            <th data-field="price"  class="center-align">Fecha creación</th>
			            <th data-field="price"  class="center-align">Fecha Modificación</th>
			            <th data-field="price"  class="center-align">Modificar</th>
			            <th data-field="price"  class="center-align">Eliminar</th>
		          	</tr>
		        </thead>
			    <tbody>
			    	@foreach ($maquinas as $maquina)
			          	<tr  class="center-align">
				            <td  class="center-align">{{$maquina->id}}</td>
				            <td  class="center-align">{{$maquina->nombre}}</td>
				            <td  class="center-align">{{$maquina->factor}}</td>
				            <td  class="center-align">{{$maquina->created_at}}</td>
				            <td  class="center-align">{{$maquina->updated_at}}</td>
				            <td  class="center-align">
											{!! link_to_route('maquinas.edit', $title = 'Editar', $parameters = $maquina->id, $attributes = array('class'=>'btn ')) !!}
										</td>
				            <td  class="center-align">
											{!! link_to_route('maquinas.edit', $title = 'Borrar', $parameters = $maquina->id, $attributes = array('class'=>'btn red darken-3')) !!}
										</td>
			          	</tr>
					@endforeach
			    </tbody>
	      </table>
      </div>
    </div>
    <div class="col s12 center-align">
        <h6 class="bold">Cantidad registros: {!! $maquinas->total() !!}</h6>
    	{!! $maquinas->render() !!}
    </div>
@stop
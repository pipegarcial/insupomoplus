<div class="col s12 m12 l6 offset-l3 center-align">
    <div class="input-field col s12 center-align">
        {!! Form::text('nombre',null, array('class' => 'validate','id' => 'nombre-maquina')) !!}
        {!! Form::label('nombre-maquina', 'Nombre maquina...')  !!}

        {!! $errors->first('nombre', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="input-field col s12 center-align">
        {!! Form::text('factor',null, array('class' => 'validate','id' => 'factor-maquina')) !!}
        {!! Form::label('factor-maquina', 'Factor...')  !!}

        {!! $errors->first('factor', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="col s12 m12 l12 center-align">
        {!! Form::submit($titleBtn,array("class" => "btn margin-btn-form btn-40")) !!}
    </div>

    <div class="col s12 m12 l12 center-align">
       {!! link_to('maquinas', $title = 'Volver', $parameters = array("class" => "btn margin-btn-form btn-40"), 
       $attributes = array()) !!}
    </div>
</div>
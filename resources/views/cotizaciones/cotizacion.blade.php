@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Cotizaciones</h5>
        </div>


        <div class="s12 center-align">
        {!! link_to('cotizaciones/create', $title = 'Agregar Cotización', $parameters = array("class" => "btn margin-btn-form"), 
       $attributes = array()) !!}
        </div>
    </div>
     <div class="row">
    	@if(Session::has('message'))
     		<div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso">
               {{Session::get('message')}}           
        	</div>
        @endif 
	
		<div class="col s12 center-align">
           <h6 class="bold">Cantidad registros: {!! $cotizaciones->total() !!}</h6>
    		{!! $cotizaciones->render() !!}
        </div>
     
		
		<div class="col s12 ">
	        <table>
		        <thead >
		          	<tr>
			            <th data-field="id"  class="center-align">Consecutivo</th>
			            <th data-field="name"  class="center-align">Nombre</th>
			            <th data-field="price"  class="center-align">Cliente</th>
			            <th data-field="price"  class="center-align">Fecha Creación</th>
									<th data-field="price"  class="center-align">Usuario Creador</th>
			            <th data-field="price"  class="center-align">PDF</th>
			            <th data-field="price"  class="center-align">Eliminar</th>
		          	</tr>
		        </thead>
			    <tbody>
			    	@foreach ($cotizaciones as $cotizacion)
			          	<tr  class="center-align">
				            <td  class="center-align">{{$cotizacion->id}}</td>
				            <td  class="center-align">{{$cotizacion->nombre}}</td>
				            <td  class="center-align">{{$cotizacion->cliente}}</td>
				            <td  class="center-align">{{$cotizacion->created_at}}</td>
										<td  class="center-align">{{$cotizacion->usuario_creador}}</td>
				            <td  class="center-align">
												{!! link_to_route('cotizaciones.edit', $title = 'PDF', $parameters = $cotizacion->id, $attributes = array('class'=>'btn ')) !!}</td>
				  <td  class="center-align">{!! link_to_route('cotizaciones.edit', $title = 'Borrar', $parameters = $cotizacion->id, $attributes = array('class'=>'btn red darken-3')) !!}</td>
			          	</tr>
					@endforeach
			    </tbody>
	      </table>
      </div>
    </div>
    <div class="col s12 center-align">
        <h6 class="bold">Cantidad registros: {!! $cotizaciones->total() !!}</h6>
    	{!! $cotizaciones->render() !!}
    </div>
@stop
@extends('layouts.main_app')

@section('content')
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear Cotización</h5>
        </div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Cotización creado'])

        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'cotizaciones.store','method' => 'POST','class'=>'col s12')) !!}
                @include('cotizaciones.include_form-cotizacion',['titleBtn' => 'Crear'])
             {!! Form::close() !!}
    	</div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Cotización creado'])
    </div>
@stop
@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Editar Cotización</h5>
        </div>

        <div class="col s12 center-align">
            {!! Form::model($cotizaciones,array('route' => ['cotizaciones.update',$cotizaciones->id],'method'=>'PUT','class'=>'col s12')) !!}

                @include('cotizaciones.include_form-cotizacion',['titleBtn' => 'Modificar'])

                {!! Form::close() !!}

    	</div>

        <div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso center-align">
            @if(Session::has('message'))
                <h6>Cotización editado </h6>       
            @endif      
        </div>
    </div>
@stop
<!-- ================================================== -->
<!-- Macro create for use collection-->
{!! Form::macro( 'DBSelect', function($fieldName,$collection,$options=array('code' => 'name'), $id){ $key = key($options); $rows = $collection->lists($options[$key], $key ); return Form::select($fieldName,$rows,null,array('class' => 'validate','id' =>
$id)); }) !!}

<div class="col s12 m12 l10 offset-l1 center-align">
  <!-- ========================================  -->
  <!-- Nombre  -->
  <!-- ========================================  -->
  <div class="input-field col s6 center-align">
    {!! Form::text('nombre_cotizacion',null, array('class' => 'validate','id' => 'nombre-cotizacion')) !!} {!! Form::label('nombre_cotizacion', 'Nombre...') !!} {!! $errors->first('nombre_cotizacion', "<span class='error center-align'>:message</span>")!!}
  </div>
  <!-- ========================================  -->
  <!-- Cliente  -->
  <!-- ========================================  -->
  <div class="col s6 center-align">
    <div class="input-field col s6 padding-left-0">
      {!! Form::DBSelect('clientes',$clientes,array('id' => 'nombre'),'clientes-id-cotizacion') !!} {!! Form::label('clientes-id-cotizacion', 'Clientes...') !!} {!! $errors->first('clientes', "<span class='error'>:message</span>") !!}
    </div>
    <div class="input-field col s6 padding-right-0">
      <input id="serch-cliente-cotiza" type="text" class="validate search" data-id-select="clientes-id-cotizacion">
      <label for="serch-cliente-cotiza">Buscar clientes</label>
    </div>
  </div>
  <!-- ========================================  -->
  <!-- Referencia  -->
  <!-- ========================================  -->
  <div class="col s6 center-align">
    <div class="input-field col s6 padding-left-0">
      {!! Form::DBSelect('referencia',$referencias,array('id' => 'nombre'),'referencia-id-cotizacion') !!} {!! Form::label('referencia-id-cotizacion', 'Referencia...') !!} {!! $errors->first('referencia', "<span class='error'>:message</span>") !!}
    </div>
    <div class="input-field col s6 padding-right-0">
      <input id="serch-referencia-cotiza" type="text" class="validate search" data-id-select="referencia-id-cotizacion">
      <label for="serch-referencia-cotiza">Buscar en cat referencia</label>
    </div>
  </div>
  <!-- ========================================  -->
  <!-- Cantidad  -->
  <!-- ========================================  -->
  <div class="input-field col s6 center-align">
    {!! Form::text('cantidad_cotizacion',null, array('class' => 'validate','id' => 'cantidad-cotizacion')) !!} {!! Form::label('cantidad_cotizacion', 'Cantidad...') !!} {!! $errors->first('cantidad_cotizacion', "<span class='error center-align'>:message</span>")!!}
  </div>

  <!-- ========================================  -->
  <!-- ========================================  -->
  <div class="col s12">
    <ul class="tabs">
      <li class="tab col s3">
        <a href="#piezas-cot">Piezas</a>
      </li>
      <li class="tab col s3">
        <a href="#insumos-cot">Insumos</a>
      </li>
      <li class="tab col s3">
        <a href="#confecciones-cot">Confecciones</a>
      </li>
      <li class="tab col s3">
        <a href="#oficios-cot">Oficios Varios</a>
      </li>
    </ul>
  </div>

  <div id="piezas-cot" class="col s12">
    <div id="" class="col s12 margin-normal">
      <table>
        <thead>
          <tr>
            <th data-field="name" class="center-align">Nombre</th>
            <th data-field="name" class="center-align">Material</th>
            <th data-field="price" class="center-align">Color</th>
            <th data-field="price" class="center-align">Cantidad</th>
            <th data-field="price" class="center-align">Ancho</th>
            <th data-field="price" class="center-align">Largo</th>
          </tr>
        </thead>
        <tbody id="piezas-tbody">
         
        </tbody>
      </table>
    </div>
  </div>

  <div id="insumos-cot" class="col s12">
    <div id="" class="col s12 margin-normal">
      <table>
        <thead>
          <tr>
            <th data-field="name" class="center-align">Insumo</th>
            <th data-field="name" class="center-align">Cantidad</th>
            <th data-field="price" class="center-align">Consumo</th>
            <th data-field="price" class="center-align">Observaciones</th>
          </tr>
        </thead>
        <tbody id="insumos-tbody"> 
        
        </tbody>
      </table>
    </div>
  </div>

    <div id="confecciones-cot" class="col s12">
      <div id="" class="col s12 margin-normal">
        <table>
          <thead>
            <tr>
              <th data-field="name" class="center-align">Proceso</th>
              <th data-field="name" class="center-align">Maquina</th>
              <th data-field="name" class="center-align">Recorrido</th>
              <th data-field="price" class="center-align">Cantidad</th>
            </tr>
          </thead>
          <tbody id="confeccion-tbody"> 
          </tbody>
        </table>
      </div>
    </div>

    <div id="oficios-cot" class="col s12">
      <div id="" class="col s12 margin-normal">
        <table>
          <thead>
            <tr>
              <th data-field="name" class="center-align">Proceso</th>
              <th data-field="name" class="center-align">Oficios</th>
              <th data-field="price" class="center-align">Cantidad</th>
            </tr>
          </thead>
          <tbody id="oficio-tbody"> 
          </tbody>
        </table>
      </div>
    </div>
    <!-- ========================================  -->
    <!-- ========================================  -->
    <div class="col s12">
      <hr class="orange-line" />
    </div>
    <!-- ========================================  -->
    <div class="col s12 left-align subtitles">
      <h5>Agregar Items:</h5>
    </div>
    <div class="col s12">
      <div class="col s6 padding-left-0">
        <!-- ========================================  -->
        <div class="input-field col s6 center-align padding-left-0">
          {!! Form::text('item_nombre_1',null, array('class' => 'validate','id' => 'item-nombre-1')) !!} {!! Form::label('item-nombre-1', 'Nombre...') !!} {!! $errors->first('item_nombre_1', "<span class='error center-align'>:message</span>")!!}
        </div>
        <div class="input-field col s6 center-align padding-right-0">
          {!! Form::text('item_valor_1',0,array('class' => 'validate','id' => 'item-valor-1')) !!} {!! Form::label('item-valor-1', 'Valor...') !!} {!! $errors->first('item_valor_1', "<span class='error center-align'>:message</span>")!!}
        </div>
        <!-- ========================================  -->
        <div class="input-field col s6 center-align padding-left-0">
          {!! Form::text('item_nombre_2',null, array('class' => ' validate','id' => 'item-nombre-2')) !!} {!! Form::label('item-nombre-2', 'Nombre...') !!} {!! $errors->first('item_nombre_2', "<span class='error center-align'>:message</span>")!!}
        </div>

        <div class="input-field col s6 center-align padding-right-0">
          {!! Form::text('item_valor_2',0,array('class' => ' validate','id' => 'item-valor-2')) !!} {!! Form::label('item-valor-2', 'Valor...') !!} {!! $errors->first('item_valor_2', "<span class='error center-align'>:message</span>")!!}
        </div>
        <!-- ========================================  -->
        <div class="input-field col s6 center-align padding-left-0">
          {!! Form::text('item_nombre_3',null, array('class' => 'validate','id' => 'item-nombre-3')) !!} {!! Form::label('item-nombre-3', 'Nombre...') !!} {!! $errors->first('item_nombre_3', "<span class='error center-align'>:message</span>")!!}
        </div>
        <div class="input-field col s6 center-align padding-right-0">
          {!! Form::text('item_valor_3',0,array('class' => 'validate','id' => 'item-valor-3')) !!} {!! Form::label('item-valor-3', 'Valor...') !!} {!! $errors->first('item_valor_3', "<span class='error center-align'>:message</span>")!!}
        </div>
        <!-- ========================================  -->
      </div>
      <div class="col s6 padding-right-0">
        <!-- ========================================  -->
        <div class="input-field col s6 center-align padding-left-0">
          {!! Form::text('item_nombre_4',null, array('class' => 'validate','id' => 'item-nombre-4')) !!} {!! Form::label('item-nombre-4', 'Nombre...') !!} {!! $errors->first('item_nombre_4', "<span class='error center-align'>:message</span>")!!}
        </div>

        <div class="input-field col s6 center-align padding-right-0">
          {!! Form::text('item_valor_4',0,array('class' => ' validate','id' => 'item-valor-4')) !!} {!! Form::label('item-valor-4', 'Valor...') !!} {!! $errors->first('item_valor_4', "<span class='error center-align'>:message</span>")!!}
        </div>
        <!-- ========================================  -->
        <div class="input-field col s6 center-align padding-left-0">
          {!! Form::text('item_nombre_5',null, array('class' => 'validate','id' => 'item-nombre-5')) !!} {!! Form::label('item-nombre-5', 'Nombre...') !!} {!! $errors->first('item_nombre_5', "<span class='error center-align'>:message</span>")!!}
        </div>
        <div class="input-field col s6 center-align padding-right-0">
          {!! Form::text('item_valor_5',0,array('class' => 'validate','id' => 'item-valor-5')) !!} {!! Form::label('item-valor-5', 'Valor...') !!} {!! $errors->first('item_valor_5', "<span class='error center-align'>:message</span>")!!}
        </div>
        <!-- ========================================  -->
        <div class="input-field col s6 center-align padding-left-0">
          {!! Form::text('item_nombre_6',null, array('class' => 'validate','id' => 'item-nombre-6')) !!} {!! Form::label('item-nombre-6', 'Nombre...') !!} {!! $errors->first('item_nombre_6', "<span class='error center-align'>:message</span>")!!}
        </div>
        <div class="input-field col s6 center-align padding-right-0">
          {!! Form::text('item_valor_6',0,array('class' => 'validate','id' => 'item-valor-6')) !!} {!! Form::label('item-valor-6', 'Valor...') !!} {!! $errors->first('item_valor_6', "<span class='error center-align'>:message</span>")!!}
        </div>
        <!-- ========================================  -->
      </div>
    </div>
    <!-- ========================================  -->
    <div class="col s12">
      <hr class="orange-line" />
    </div>
    <!-- ========================================  -->
    <div class="valign-wrapper col s12">
      <div class="col s6 eft-align subtitles">
        <a id="btn-calcular" class="waves-effect waves-light btn"><i class="material-icons left">cloud</i>Calcular</a>
      </div>

      <div class="col s6 eft-align">
        <h2>$<span id="calculo">0.00</span></h2>
      </div>
    </div>

    <!-- ========================================  -->
    <div class="col s12">
      <hr class="orange-line" />
    </div>
    <!-- ========================================  -->
    <!-- ========================================  -->
    <div class="col s12 m12 l12 center-align">
      {!! Form::submit($titleBtn,array("class" => "btn margin-btn-form btn-40")) !!}
    </div>

    <div class="col s12 m12 l12 center-align">
      {!! link_to('cotizaciones', $title = 'Volver', $parameters = array("class" => "btn margin-btn-form btn-40"), $attributes = array()) !!}
    </div>
  </div>
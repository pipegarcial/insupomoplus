@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Laminas</h5>
        </div>

        <div class="s12 center-align">
           {!! link_to('laminas/create', $title = 'Agregar Lamina', $parameters = array("class" => "btn margin-btn-form"), 
       		$attributes = array()) !!}
        </div>
        
    </div>
     <div class="row">
    	@if(Session::has('message'))
     		<div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso">
               {{Session::get('message')}}           
        	</div>
        @endif 
	
		<div class="col s12 center-align">
           <h6 class="bold">Cantidad registros: {!! $laminas->total() !!}</h6>
    		{!! $laminas->render() !!}
        </div>

		<div class="col s12 ">
	        <table>
		        <thead >
		          	<tr>
			            <th data-field="id"  class="center-align">Código</th>
			            <th data-field="name"  class="center-align">Nombre</th>
			            <th data-field="name"  class="center-align">Ancho</th>
			            <th data-field="name"  class="center-align">Largo</th>
			            <!--<th data-field="price"  class="center-align">Fecha creación</th>-->
			            <th data-field="price"  class="center-align">Precio</th>
									<th data-field="price"  class="center-align">Última Modificación</th>
			            <th data-field="price"  class="center-align">Modificar</th>
			            <th data-field="price"  class="center-align">Eliminar</th>
		          	</tr>
		        </thead>
			    <tbody>
			    	@foreach ($laminas as $lamina)
			          	<tr  class="center-align">
				            <td  class="center-align">{{$lamina->id}}</td>
				            <td  class="center-align">{{$lamina->nombre}}</td>
				            <td  class="center-align">$ {{$lamina->ancho}}</td>
				            <td  class="center-align">{{$lamina->largo}}</td>
										<td  class="center-align">{{$lamina->precio}}</td>
										<td  class="center-align">{{$lamina->updated_at}}</td>
				            <td  class="center-align ver-proveedores" data-proveedor="lamina" data-ver="{{$lamina->id}}"  data-nombre="{{$lamina->nombre}}" data-tabla="laminas" data-tabla-rel="laminas_proveedores" data-id-param="id_lamina" ><a class=" waves-effect waves-light btn">Proveedores</a></td>
				    				<td class="center-align"> {!! link_to_route('laminas.edit', $title = 'Editar', $parameters = $lamina->id, $attributes = array('class'=>'btn ')) !!}</td>
				  					<td  class="center-align">{!! link_to_route('laminas.edit', $title = 'Borrar', $parameters = $lamina->id, $attributes = array('class'=>'btn red darken-3')) !!}</td>
			          	</tr>
					@endforeach
			    </tbody>
	      	</table>
      	</div>
    </div>
  
    <div class="col s12 center-align">
        <h6 class="bold">Cantidad registros: {!! $laminas->total() !!}</h6>
    	{!! $laminas->render() !!}
    </div>

  <!-- Modal Structure -->
  <div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content center-align">
			   <h5  class="important regular bold" id="titulo-material"> </h5>
     		 <h5  class="">Lista de Proveedores</h5>
       	<table>
					<thead >
							<tr>
								<th data-field="id"  class="center-align">Código</th>
								<th data-field="name"  class="center-align">Empresa</th>
								<th data-field="name"  class="center-align">Direccion</th>
								<th data-field="price"  class="center-align">Teléfono</th>
								<th data-field="price"  class="center-align">Celular</th>
								<th data-field="price"  class="center-align">Email</th>
							</tr>
					</thead>
			    <tbody id="result-materiales">
			    </tbody>
	      </table>
    </div>
		<div class="modal-footer center-align">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
	</div>
@stop
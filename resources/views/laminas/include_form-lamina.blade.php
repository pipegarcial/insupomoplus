<!-- ================================================== -->
<!-- Macro create for use collection from model Country -->
{!! Form::macro( 'DBSelect', function($fieldName, $collection, $options=array('id' => 'empresa')){ $key = key($options); $rows = $collection->lists($options[$key], $key ); return Form::select($fieldName, $rows,null,array('class' => 'validate select-custom','id'
=> 'icon_country'),array('multiple')); }) !!}


<div class="col s12 m12 l6 offset-l3 center-align">

  <div class="input-field col s12 center-align">
    {!! Form::text('nombre',null, array('class' => 'validate','id' => 'nombre')) !!} {!! Form::label('nombre', 'Nombre lamina...') !!} {!! $errors->first('nombre', "<span class='error center-align'>:message</span>") !!}
  </div>

  <div class="input-field col s12 center-align">
    {!! Form::text('ancho',null, array('class' => 'validate','id' => 'ancho')) !!} {!! Form::label('ancho', 'Ancho lamina...') !!} {!! $errors->first('ancho', "<span class='error center-align'>:message</span>") !!}
  </div>

  <div class="input-field col s12 center-align">
    {!! Form::text('largo',null, array('class' => 'validate','id' => 'largo')) !!} {!! Form::label('largo', 'Largo lamina...') !!} {!! $errors->first('largo', "<span class='error center-align'>:message</span>") !!}
  </div>

  <div class="input-field col s12 center-align">
    {!! Form::text('precio',null, array('class' => 'validate','id' => 'precio')) !!} {!! Form::label('precio', 'Precio lamina...') !!} {!! $errors->first('precio', "<span class='error center-align'>:message</span>") !!}
  </div>

  <!-- ================================================== -->
  <!-- Proveedores -->
  <div class="input-field col s7">
    <select id="prov-laminas" multiple name="proveedores-list[]" class='custom-scroll'>
      <option value="" disabled selected>Escoge el o los proveedores</option>
      @foreach($proveedores as $proveedor)
      <option value='{{$proveedor->id}}'>{{$proveedor->empresa}}</option>
      @endforeach
    </select>

    {!! $errors->first('proveedores-list[]', "<span class='error'>:message</span>") !!}
  </div>
  <div class="input-field col s5">
    <input id="serch-prov-laminas" type="text" class="validate search-dina" data-nombre="laminas" data-id-select="prov-laminas">
    <label for="serch-material">Buscar proveedors</label>
  </div>
  <!--
  <div class="input-field col s5">
    <input id="serch-pro-laminas" type="text" class="validate search" data-id-select="prov-laminas">
    <label for="serch-pro-laminas">Buscar en material</label>
  </div>
  -->
  <div class="col s12 m12 l6 center-align">
    {!! link_to('laminas', $title = 'Volver', $parameters = array("class" => "btn margin-btn-form btn-40"), $attributes = array()) !!}
  </div>

  <div class="col s12 m12 l6 center-align">
    {!! Form::submit($titleBtn,array("class" => "btn margin-btn-form btn-40")) !!}
  </div>

</div>
@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear Laminas</h5>
        </div>
      @include('includes.include_mensaje-creado',['mensaje' => 'Material creado'])
        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'laminas.store','method' => 'POST','class'=>'col s12')) !!}

                @include('laminas.include_form-lamina',['titleBtn' => 'Crear'])

            {!! Form::close() !!}

    	</div>

       @include('includes.include_mensaje-creado',['mensaje' => 'Material creado'])
    </div>
@stop
@extends('layouts.main_app')


@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Editar referencia</h5>
        </div>
         @include('includes.include_mensaje-creado',['mensaje' => 'Referencia editada'])
        <div class="col s12 center-align">
            {!! Form::model($referencias,array('route' => ['referencias.update',$referencias->id],'files'=>true,'method'=>'PUT','class'=>'col s12')) !!}

                @include('referencias.include_form-referencia',['titleBtn' => 'Modificar','displayClass'=>'display','url' => 'http://codespipe.com/proyectos/insupomoplus-copia/'.$referencias->imagen])

            {!! Form::close() !!}
            
     
    	  </div>
         @include('includes.include_mensaje-creado',['mensaje' => 'Referencia editada'])
    </div>
@stop
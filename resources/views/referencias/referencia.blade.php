@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Referencias</h5>
        </div>

        <div class="s12 center-align">
           {!! link_to('referencias/create', $title = 'Agregar referencia', $parameters = array("class" => "btn margin-btn-form"), 
       		$attributes = array()) !!}
        </div>
        
    </div>
     <div class="row">
    	@if(Session::has('message'))
     		<div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso">
               {{Session::get('message')}}           
        	</div>
        @endif 
	
		<div class="col s12 center-align">
           <h6 class="bold">Cantidad registros: {!!$referencias->total() !!}</h6>
    		{!! $referencias->render() !!}
        </div>

		<div class="col s12 ">
	        <table>
		        <thead >
		          	<tr>
			            <th data-field="id"  class="center-align">Id</th>
			            <th data-field="name"  class="center-align">Consecutivo</th>
			            <th data-field="name"  class="center-align">Nombre</th>
			            <th data-field="name"  class="center-align">Cat Ref</th>
			            <!--<th data-field="price"  class="center-align">Fecha creación</th>-->

			            <th data-field="name"  class="center-align">Troquel</th>
			            <th data-field="name"  class="center-align">Pre Sug</th>
			            <th data-field="price"  class="center-align">Modificado</th>
									<th data-field="price"  class="center-align">Ver</th>
									<th data-field="price"  class="center-align">Insupros</th>
			            <th data-field="price"  class="center-align">Modificar</th>
			            <th data-field="price"  class="center-align">Eliminar</th>
		          	</tr>
		        </thead>
			    <tbody>
			    	@foreach ($referencias as $referencia)
			          	<tr  class="center-align">
				            <td  class="center-align">{{$referencia->id}}</td>
				            <td  class="center-align">{{$referencia->consecutivo}}</td>
				            <td  class="center-align">{{$referencia->nombre}}</td>
				            <td  class="center-align">{{$referencia->catnombre}}</td>
				            <!--<td  class="center-align">{{$referencia->created_at}}</td>-->
				            <td  class="center-align">{{$referencia->troquel}}</td>
				            <td  class="center-align">${{$referencia->precio_sugerido}}</td>
				            <td  class="center-align">{{$referencia->updated_at}}</td>
										<td id="algo" class="center-align ver-img-referencia" data-ver="{{$referencia->id}}"  data-nombre="{{$referencia->nombre}}"><a class=" waves-effect waves-light btn" >Ver</a></td>
				           	
										<td  class="center-align ver-insupros" data-ver="{{$referencia->id}}" data-id-param="id_referencia" ><a class=" waves-effect waves-light btn">Insu</a></td>

										<td  class="center-align">
												{!! link_to_route('referencias.edit', $title = 'Editar', $parameters = $referencia->id, $attributes = array('class'=>'btn ')) !!}</td>
				  <td  class="center-align">{!! link_to_route('referencias.edit', $title = 'Borrar', $parameters = $referencia->id, $attributes = array('class'=>'btn red darken-3')) !!}</td>
			          	</tr>
					@endforeach
			    </tbody>
	      	</table>
      	</div>
    </div>
  
    <div class="col s12 center-align">
        <h6 class="bold">Cantidad registros: {!! $referencias->total() !!}</h6>
    	{!! $referencias->render() !!}
    </div>


	
  <!-- Modal Structure IMG-->
  <div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content center-align">
			<h5  class="important regular bold" id="titulo-img-ref"> </h5>
			<figure>
        <img id="img-ref-modal" class="responsive-img" src="" alt="">
      </figure>
    </div>
		<div class="modal-footer center-align">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
	</div>


  <!-- Modal Structure Data Insupro -->
  <div id="modal2" class="modal modal-fixed-footer">
    <div class="modal-content center-align">
			   <h5  class="important regular bold" id="titulo-material"> </h5>
       	<table>
					<thead >
							<tr>
								<th data-field="id"  class="center-align">Código</th>
								<th data-field="name"  class="center-align">Nombre</th>
								<th data-field="name"  class="center-align">Editar</th>
							</tr>
					</thead>
			    <tbody id="result-materiales">
			    </tbody>
	      </table>
    </div>
		<div class="modal-footer center-align">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
	</div>

@stop
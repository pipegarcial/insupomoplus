<!-- ================================================== -->
<!-- Macro create for use collection from model Country -->
{!! Form::macro( 'DBSelect', function($fieldName,$collection,$options=array('code' => 'name'), $id){ $key = key($options); $rows = $collection->lists($options[$key], $key ); return Form::select($fieldName,$rows,null,array('class' => 'validate','id' =>
$id)); }) !!}

<div class="col s12 m12 l6 center-align">

  <!-- ================================================== -->
  <!-- Cat Referencia -->
  <div class="input-field col s7 center-align">
    {!! Form::DBSelect('categoria_id',$catReferencias,array('id' => 'nombre'),'categoria-id') !!} {!! Form::label('categoria-id', 'Cat Referencia...') !!} {!! $errors->first('categoria-id', "<span class='error'>:message</span>") !!}
  </div>
  <div class="input-field col s5">
    <input id="serch-cat" type="text" class="validate search" data-id-select="categoria-id">
    <label for="serch-cat">Buscar en cat referencia</label>
  </div>
  <!-- ================================================== -->
  <!-- Concecutivo -->
  <div class="input-field col s12 center-align">
    {!! Form::text('consecutivo',null,array('class' => 'validate','id' => 'consecutivo','placeholder'=>'','readonly')) !!} {!! Form::label('', 'Concecutivo...') !!} {!! $errors->first('consecutivo', "<span class='error center-align'>:message</span>")
    !!}
  </div>

  <!-- ================================================== -->
  <!-- Nombre -->
  <div class="input-field col s12 center-align">
    {!! Form::text('nombre',null, array('class' => 'validate','id' => 'nombre-referencia')) !!} {!! Form::label('nombre-referencia', 'Nombre...') !!} {!! $errors->first('nombre', "<span class='error center-align'>:message</span>") !!}
  </div>
  <!-- ================================================== -->
  <!-- Material -->
  <div class="input-field col s7 center-align">
    {!! Form::DBSelect('material_id', $materiales, array('id' => 'nombre'),'material-id') !!} {!! Form::label('material-id', 'Material...') !!} {!! $errors->first('material', "<span class='error'>:message</span>") !!}
  </div>
  <div class="input-field col s5">
    <input id="serch-material" type="text" class="validate search" data-id-select="material-id">
    <label for="serch-material">Buscar en material</label>
  </div>
  <!-- ================================================== -->
  <!-- Troquel -->
  <div class="input-field col s12 center-align">
    {!! Form::select('troquel', array('si' => 'Si', 'no' => 'No'), null) !!} {!! Form::label('troquel', 'Troquel...') !!} {!! $errors->first('troquel', "<span class='error center-align'>:message</span>") !!}
  </div>

  <!-- ================================================== -->
  <!-- Categoria -->
  <!-- <div class="input-field col s12 center-align">
        {!! Form::select('categoria', array(1 => 'Insumo', 2 => 'Material',3 => 'Referencia'),null, ['id' => 'categoria-id']) !!}
        {!! Form::label('categoria-id', 'Categoria...')  !!}

        {!! $errors->first('categoria', "<span class='error center-align'>:message</span>") !!}
    </div> -->
  <!-- ================================================== -->
  <!-- Elementos Categoria -->
  <!-- <div id="wrapper-elementos" class="input-field col s12 center-align">
       {!! Form::select('nombre_categoria',['placeholder' => 'Seleccionar...'],null,['id'=>'elementos-categoria']) !!}
        {!! Form::label('elementos-categoria', 'Elemento categoria...')  !!}

        {!! $errors->first('nombre_categoria', "<span class='error center-align'>:message</span>") !!}
    </div> -->
  <!-- ================================================== -->
  <!-- Precio Sugerido -->
  <div class="input-field col s12 center-align">
    {!! Form::text('precio_sugerido',null, array('class' => 'validate','id' => 'precio')) !!} {!! Form::label('precio', 'Precio sugerido...') !!} {!! $errors->first('precio_sugerido', "<span class='error center-align'>:message</span>") !!}
  </div>
</div>

<div class="input-field col s12 m6 center-align">
  {!! Form::text('largo',null, array('class' => 'validate','id' => 'largo-insupro')) !!} {!! Form::label('largo-insupro', 'Largo...') !!} {!! $errors->first('largo_basico_insupro', "<span class='error center-align'>:message</span>") !!}
</div>
<!-- ===================================== -->
<div class="input-field col s12 m6 center-align">
  {!! Form::text('ancho',null, array('class' => 'validate','id' => 'ancho-insupro')) !!} {!! Form::label('ancho-insupro', 'Ancho...') !!} {!! $errors->first('ancho_basico_insupro', "<span class='error center-align'>:message</span>") !!}
</div>
<!-- ===================================== -->
<div class="input-field col s12 m6 center-align">
  {!! Form::text('profundidad',null, array('class' => 'validate','id' => 'profunidad-insupro')) !!} {!! Form::label('profunidad-insupro', 'Profundidad...') !!} {!! $errors->first('profundidad_basico_insupro', "<span class='error center-align'>:message</span>")
  !!}
</div>
<!-- ===================================== -->
<!-- Foto ================================ -->
<div class="col s12 m12 l6 center-align">
  <div class="input-field col s12">
    <div class="file-field col s12 center-align">
      <div class="btn">
        <span>Foto</span> {!! Form::file('imagen',null, array('class' => 'file','id' => 'image-insupro')) !!}
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text">
      </div>
      {!! $errors->first('imagen', "<span class='error center-align'>:message</span>") !!}
    </div>
  </div>
  <div class="col s12 center-align">
    <figure class="{{$displayClass}} ">
      <img class="responsive-img" src="{{$url}}" alt="">
    </figure>
  </div>
  <div>
    <figure>
      <img src="" alt="">
    </figure>
  </div>
</div>
<!-- ===================================== -->
<div>
  <div class="col s12 m12 l12 center-align">
    <div class="col s12 m12 l6 center-align">
      {!! link_to('referencias', $title = 'Volver', $parameters = array("class" => "btn margin-btn-form btn-40 col s12"), $attributes = array()) !!}
    </div>

    <div class="col s12 m12 l6 center-align">
      {!! Form::submit($titleBtn,array("class" => "btn margin-btn-form btn-40 col s12")) !!}
    </div>
  </div>
  <div class="col s12 m12 l6 center-align">
@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
      @include('includes.include_mensaje-creado',['mensaje' => 'Referencia creado'])
        <div class="col s12 center-align">
            <h5 class="important regular">Crear referencia</h5>
        </div>

        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'referencias.store','method' => 'POST','class'=>'col s12','files'=>true)) !!}

                @include('referencias.include_form-referencia',['titleBtn' => 'Crear','displayClass'=>'','url'=>''])
                

                {!! Form::close() !!}

    	</div>

       @include('includes.include_mensaje-creado',['mensaje' => 'Referencia creado'])
    </div>
@stop
@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Editar oficio</h5>
        </div>
         @include('includes.include_mensaje-creado',['mensaje' => 'Oficio creada'])
        <div class="col s12 center-align">
            {!! Form::model($oficios,array('route' => ['oficios.update',$oficios->id],'method'=>'PUT','class'=>'col s12')) !!}

                @include('oficios.include_form-oficio',['titleBtn' => 'Modificar'])

                {!! Form::close() !!}

    	</div>
         @include('includes.include_mensaje-creado',['mensaje' => 'Oficio creada'])
    </div>
@stop
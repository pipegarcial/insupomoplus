<footer class="page-footer relative">
  <div class="container">
    <div class="row">
      <div class="col l12 s12 center-align wrapper-text-footer">
        <h6 class="white-text">Promoformas SAS ® 2014 © - Todos los Derechos res</h6>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container center-align">
    Cali - Colombia
    </div>
  </div>
 </footer>
<!-- ================================================================== -->
<!-- ================================================================== -->
<!-- Scripts -->
{!! Html::script('assets/lib/bower_components/jquery/dist/jquery.min.js') !!}

{!! Html::script('assets/lib/bower_components/Materialize/dist/js/materialize.min.js') !!}

{!! Html::script('assets/js/app.js') !!}
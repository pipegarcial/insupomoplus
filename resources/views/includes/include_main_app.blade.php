
<!-- =========================== -->

<ul id="dropdownConf" class="dropdown-content">
    <li><a href="#!">Proveedores</a></li>
    <li class="divider"></li>
    <li><a href="#!">Unidad de medida</a></li>
    <li class="divider"></li>
    <li><a href="#!">Materiales</a></li>
    <li class="divider"></li>
    <li><a href="#!">Maquinas</a></li>
    <li class="divider"></li>
    <li><a href="#!">Categorías</a></li>
    <li class="divider"></li>
    <li><a href="#!">Marcas</a></li>
    <li class="divider"></li>
    <li><a href="#!">Oficios</a></li>
    <li class="divider"></li>
    <li><a href="#!">Color</a></li>
</ul>

<ul id="dropdownCount" class="dropdown-content">
    <li> {!! link_to('user', $title = 'Admin', $parameters = array(), $attributes = array()) !!}</li>
    <li> {!! link_to('logout', $title = 'Salir', $parameters = array(), $attributes = array()) !!}</li>
</ul>

<!-- =========================== -->


<nav class="nav-main-app">
    <div class="nav-wrapper">
       <div class="left">
            <figure class="figure-logo-main-app">
                    {!! Html::image('assets/img/logo/INSUPROMO.png', 'foto_leyenda', array('class' => 'responsive-img logo-main-app'))
                    !!}
            </figure>
            <figure class="figure-logo-main-app">
                    {!! Html::image('assets/img/logo/PROMOFORMAS.png','foto_leyenda', array('class' => 'responsive-img logo-main-app'))
                    !!}
            </figure>
        </div>
        <ul class="right hide-on-med-and-down">
             <li><a class="dropdown-button" href="#!" data-activates="dropdownConf">Configuración<i class="material-icons right">arrow_drop_down</i></a></li>
            <li><a href="badges.html">Referencias</a></li>
            <li><a href="badges.html">Insupro</a></li>
            <li><a href="badges.html">Cotización</a></li>
            <li><a href="badges.html">Insu Producción</a></li>
            <li><a class="dropdown-button" href="#!" data-activates="dropdownCount">Cuenta<i class="material-icons right">arrow_drop_down</i></a></li>
        </ul>
    </div>
</nav>
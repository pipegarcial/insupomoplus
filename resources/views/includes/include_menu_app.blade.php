
<!-- =========================== -->
<ul id="dropdownConf" class="dropdown-content">
    <li> {!! link_to('proveedores', $title = 'Proveedores', $parameters = array(), $attributes = array()) !!}</li>
    <li> {!! link_to('clientes', $title = 'Clientes', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
    <li class="divider"></li>
    <!-- ================================= -->
    <li> {!! link_to('unidades-medida', $title = 'Unidad de medida', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
    <li class="divider"></li>
    <!-- ================================= -->
    <li> {!! link_to('materiales', $title = 'Materiales', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
    <li class="divider"></li>
    <!-- ================================= -->
    <li> {!! link_to('maquinas', $title = 'Maquinas', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
    <li class="divider"></li>
    <li> {!! link_to('categoria-referencia', $title = 'Cat Referencia', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
    <li class="divider"></li>
    <!-- ================================= -->
    <li> {!! link_to('categoria-insumo', $title = 'Cat Insumo', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
    <li class="divider"></li>
    <!-- ================================= -->
    <li> {!! link_to('categoria-material', $title = 'Cat Material', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
    <li class="divider"></li>
    <!-- ================================= -->
    <li> {!! link_to('marcas', $title = 'Marcas', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
    <li class="divider"></li>
    <!-- ================================= -->
    <li> {!! link_to('oficios', $title = 'Oficios', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
    <li class="divider"></li>
    <!-- ================================= -->
    <li> {!! link_to('colores', $title = 'Colores', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
    <!-- ================================= -->
    <li> {!! link_to('laminas', $title = 'Laminas', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
    <!-- ================================= -->
    <li> {!! link_to('insumos', $title = 'Insumos', $parameters = array(), $attributes = array()) !!}</li>
    <!-- ================================= -->
</ul>

<ul id="dropdownCount" class="dropdown-content">
    <li> {!! link_to('user', $title = 'Admin', $parameters = array(), $attributes = array()) !!}</li>
    <li> {!! link_to('logout', $title = 'Salir', $parameters = array(), $attributes = array()) !!}</li>
</ul>

<!-- =========================== -->


<nav class="nav-main-app">
    <div class="nav-wrapper">
       <div class="left">
            <figure class="figure-logo-main-app">
                    {!! Html::image('assets/img/logo/INSUPROMO.png', 'foto_leyenda', array('class' => 'responsive-img logo-main-app'))
                    !!}
            </figure>
            <figure class="figure-logo-main-app">
                    {!! Html::image('assets/img/logo/PROMOFORMAS.png','foto_leyenda', array('class' => 'responsive-img logo-main-app'))
                    !!}
            </figure>
        </div>
        <ul class="right hide-on-med-and-down">
           <li> {!! link_to('inicio', $title = 'Inicio', $parameters = array(), $attributes = array()) !!}</li>
            <li><a class="dropdown-button" href="#!" data-activates="dropdownConf">Configuración<i class="material-icons right">arrow_drop_down</i></a></li>
            
            <li> {!! link_to('referencias', $title = 'Referencias', $parameters = array(), $attributes = array()) !!}</li>

            <li>{!! link_to('insupros', $title = 'Insupro', $parameters = array(), $attributes = array()) !!}</li>
            <li> {!! link_to('cotizaciones', $title = 'Cotización', $parameters = array(), $attributes = array()) !!}</li>
            <li><a href="badges.html">Insu Producción</a></li>
            <li><a class="dropdown-button" href="#!" data-activates="dropdownCount">Cuenta<i class="material-icons right">arrow_drop_down</i></a></li>
        </ul>
    </div>
</nav>
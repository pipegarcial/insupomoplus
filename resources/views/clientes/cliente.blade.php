@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Clientes</h5>
        </div>


        <div class="s12 center-align">
           {!! link_to('clientes/create', $title = 'Agregar Cliente', $parameters = array("class" => "btn margin-btn-form"), 
       $attributes = array()) !!}
        </div>
    </div>
     <div class="row">
    	@if(Session::has('message'))
     		<div class="col offset-s4 s4 center-align card-panel  light-green darken-1 exitoso">
               {{Session::get('message')}}           
        	</div>
        @endif 
	
		<div class="col s12 center-align">
           <h6 class="bold">Cantidad registros: {!! $clientes->total() !!}</h6>
    		{!! $clientes->render() !!}
        </div>
     
		
		<div class="col s12 ">
	        <table>
		        <thead >
		          	<tr>
			            <th data-field="id"  class="center-align">Cédula</th>
			            <th data-field="name"  class="center-align">Nombre</th>
			            <th data-field="name"  class="center-align">Apellidos</th>
			            <th data-field="price"  class="center-align">Dirección</th>
			            <th data-field="price"  class="center-align">Teléfono</th>
			            <th data-field="price"  class="center-align">Célular</th>
			            <th data-field="price"  class="center-align">Email</th>
			            <th data-field="price"  class="center-align">Fecha creación</th>
			            <th data-field="price"  class="center-align">Fecha Modificación</th>
			            <th data-field="price"  class="center-align">Modificar</th>
			            <th data-field="price"  class="center-align">Eliminar</th>
		          	</tr>
		        </thead>
			    <tbody>
			    	@foreach ($clientes as $cliente)
			          	<tr  class="center-align">
				            <td  class="center-align">{{$cliente->id}}</td>
				            <td  class="center-align">{{$cliente->nombre}}</td>
				            <td  class="center-align">{{$cliente->apellidos}}</td>
				            <td  class="center-align">{{$cliente->direccion}}</td>
				            <td  class="center-align">{{$cliente->telefono}}</td>
				             <td  class="center-align">{{$cliente->celular}}</td>
				             <td  class="center-align">{{$cliente->email}}</td>
				            <td  class="center-align">{{$cliente->created_at}}</td>
				            <td  class="center-align">{{$cliente->updated_at}}</td>
				            <td  class="center-align">
							{!! link_to_route('clientes.edit', $title = 'Editar', $parameters = $cliente->id,$attributes = array('class'=>'btn')) !!}</td>
				            <td  class="center-align">{!! link_to_route('clientes.edit', $title = 'Borrar', $parameters = $cliente->id, $attributes = array('class'=>'btn red darken-3')) !!}</td>
			          	</tr>
					@endforeach
			    </tbody>
	      </table>
      </div>
    </div>
    <div class="col s12 center-align">
        <h6 class="bold">Cantidad registros: {!! $clientes->total() !!}</h6>
    	{!! $clientes->render() !!}
    </div>
@stop
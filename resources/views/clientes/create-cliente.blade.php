@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear Cliente</h5>
        </div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Cliente creado'])

        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'clientes.store','method' => 'POST','class'=>'col s12')) !!}

                @include('clientes.include_form-cliente',['titleBtn' => 'Crear'])

                {!! Form::close() !!}

    	</div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Cliente creado'])
    </div>
@stop
<div class="col s12 m12 l6 offset-l3 center-align">
    <div class="input-field col s12 center-align">
        {!! Form::text('id',null, array('class' => 'validate','id' => 'id')) !!}
        {!! Form::label('id', 'Nit...')  !!}

        {!! $errors->first('id', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="input-field col s12 center-align">
        {!! Form::text('nombre',null, array('class' => 'validate','id' => 'nombre')) !!}
        {!! Form::label('nombre', 'Nombre...')  !!}

        {!! $errors->first('nombre', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="input-field col s12 center-align">
        {!! Form::text('apellidos',null, array('class' => 'validate','id' => 'apellidos')) !!}
        {!! Form::label('apellidos', 'Apellidos...')  !!}

        {!! $errors->first('apellidos', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="input-field col s12 center-align">
        {!! Form::text('direccion',null, array('class' => 'validate','id' => 'direccion')) !!}
        {!! Form::label('direccion', 'Dirección...')  !!}

        {!! $errors->first('direccion', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="input-field col s12 center-align">
        {!! Form::text('telefono',null, array('class' => 'validate','id' => 'telefono')) !!}
        {!! Form::label('telefono', 'Teléfono...')  !!}

        {!! $errors->first('telefono', "<span class='error center-align'>:message</span>") !!}
    </div>

    
    <div class="input-field col s12 center-align">
        {!! Form::text('celular',null, array('class' => 'validate','id' => 'celular')) !!}
        {!! Form::label('celular', 'Célular...')  !!}

        {!! $errors->first('celular', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="input-field col s12 center-align">
        {!! Form::text('email',null, array('class' => 'validate','id' => 'email')) !!}
        {!! Form::label('email', 'Email...')  !!}

        {!! $errors->first('email', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="col s12 m12 l12 center-align">
        {!! Form::submit($titleBtn,array("class" => "btn margin-btn-form btn-40")) !!}
    </div>

    <div class="col s12 m12 l12 center-align">
       {!! link_to('clientes', $title = 'Volver', $parameters = array("class" => "btn margin-btn-form btn-40"), 
       $attributes = array()) !!}
    </div>
</div>
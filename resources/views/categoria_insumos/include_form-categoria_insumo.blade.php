<div class="col s12 m12 l6 offset-l3 center-align">
        
    <div class="input-field col s12 center-align">
        {!! Form::text('codigo',null, array('class' => 'validate','id' => 'icon_cat_insu_codigo')) !!}
        {!! Form::label('icon_cat_insu_codigo', 'Códgio...')  !!}

        {!! $errors->first('codigo', "<span class='error center-align'>:message</span>") !!}
    </div>
    <div class="input-field col s12 center-align">
        {!! Form::text('nombre',null, array('class' => 'validate','id' => 'icon_nombre_insumo')) !!}
        {!! Form::label('icon_nombre_insumo', 'Nombre...')  !!}

        {!! $errors->first('nombre', "<span class='error center-align'>:message</span>") !!}
    </div>

    <div class="col s12 m12 l12 center-align">
        {!! Form::submit($titleBtn,array("class" => "btn margin-btn-form btn-40")) !!}
    </div>

    <div class="col s12 m12 l12 center-align">
       {!! link_to('categoria-insumo', $title = 'Volver', $parameters = array("class" => "btn margin-btn-form btn-40"), 
       $attributes = array()) !!}
    </div>
</div>
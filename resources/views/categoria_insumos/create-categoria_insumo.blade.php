@extends('layouts.main_app')

@section('content')
    <!-- Dropdown Structure -->
    <div class="row">
        <div class="col s12 center-align">
            <h5 class="important regular">Crear Elemento Categoría Insumo</h5>
        </div>

        <div class="col s12 center-align">
             {!! Form::open(array('route' => 'categoria-insumo.store','method' => 'POST','class'=>'col s12')) !!}

                @include('categoria_insumos.include_form-categoria_insumo',['titleBtn' => 'Crear']);

                {!! Form::close() !!}

        </div>

        @include('includes.include_mensaje-creado',['mensaje' => 'Elemento categoría insumo creado']);
    </div>
@stop
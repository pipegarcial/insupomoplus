<!-- ================================================== -->
<!-- Head - Header -->
@include('includes.include_head')
    <!-- ================================================== -->
    <!-- Body -->
    <body class="body-principal-views">
        <header class="row header-main-app">
               @include('includes.include_menu_app')
        </header>

            
        <section id="app-section-main" class="container section-main-app">
            <article class="row">
                @yield('content')
            </article>
        </section>
        
        <!-- ================================================== -->
        <!-- Footer -->
        @include('includes.include_footer')
    </body>
</html>
<!-- ================================================== -->
<!-- Head - Header -->
@include('includes.include_head')

    <!-- ================================================== -->
    <!-- Body -->
    <body class="body-principal-views">
        <header class="row header-main header-main">
            <figure class="col s6 right-align figure-logo-main">
                {!! Html::image('assets/img/logo/INSUPROMO.png', 'foto_leyenda', array('class' => 'responsive-img logo-main'))
                !!}
            </figure>
            <figure class="col s6 left-align figure-logo-main">
                {!! Html::image('assets/img/logo/PROMOFORMAS.png','foto_leyenda', array('class' => 'responsive-img logo-main'))
                !!}
            </figure>
        </header>

        <section class="container section-main">
            <article class="row">
                @yield('content')
            </article>
        </section>
        

        <!-- ================================================== -->
        <!-- Footer -->
        @include('includes.include_footer')

    </body>
</html>
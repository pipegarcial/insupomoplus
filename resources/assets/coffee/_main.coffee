contadorPiezas = 1
arrayPiezasInsupro = []
$(document).ready ->
	#==========================================
    # Calculando la altura del documento para 
    # acomodar el footer
    #==========================================
    if $('#app-section-main').innerHeight()>600
         $('footer').addClass('relative');
    else
         $('footer').addClass('fixed');

    $('.modal-trigger').leanModal();
    #==========================================
    # Inicializando los select de materialize
    #==========================================
    $('select').material_select()
    #==========================================
    # Inicializando los dropdown de materialize
    #==========================================
    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 300,
        constrain_width: true, 
        hover: false, 
        gutter: 0, 
        belowOrigin: true, 
        alignment: 'left' 
    })
    #==========================================
    # Inicializando el btn para el sidenav de 
    # materialize
    #==========================================
    $(".button-collapse").sideNav();
    #==========================================
    #Inicializando los dropdown de materialize
    #==========================================
    #oficios insupro checkbox
    #==========================================
    $("#piezas-insupro-contenedor").hide()
    $("#agregar-piezas:checkbox").change ->
    	$("#piezas-insupro-contenedor").slideToggle()
   	#==========================================
    #oficios insupro checkbox
    #==========================================
    $("#insumos-insupro-contenedor").hide()
    $("#agregar-insumos:checkbox").change ->
    	$("#insumos-insupro-contenedor").slideToggle()

    #==========================================
    #oficios insupro checkbox
    #==========================================
    $("#confecciones-insupro-contenedor").hide()
    $("#agregar-confecciones:checkbox").change ->
    	$("#confecciones-insupro-contenedor").slideToggle()

   	#==========================================
    #oficios insupro checkbox
    #==========================================
    $("#oficios-insupro-contenedor").hide()
    $("#agregar-oficios:checkbox").change ->
    	$("#oficios-insupro-contenedor").slideToggle() 

    $("#resultados-piezas-agregadas").hide()	
    $("#agregar-pieza").on 'click', ->
    	if $("#nombre-insupro-pieza").val() != "" && $("#material-insupro-pieza").val() != null && $("#color-insupro-pieza").val() != null && $("#cantidad-pieza").val() != "" && $("#largo-pieza").val() != "" && $("#ancho-pieza").val() != "" 
	    	$("#piezas-tbody").prepend "<tr class='center-align'><td  class='center-align'>"+contadorPiezas+"</td><td  class='center-align'>"+$("#nombre-insupro-pieza").val()+"</td><td  class='center-align'>"+$("#material-insupro-piezas").val()+"</td><td  class='center-align'>"+$("#color-insupro-piezas").val()+"</td><td  class='center-align'>"+$("#cantidad-pieza").val()+"</td><td  class='center-align'>"+$("#ancho-pieza").val()+"</td><td  class='center-align'>"+$("#largo-pieza").val()+"</td><td class='center-align'><button type='button' class='waves-effect waves-light btn red darken-4'><i class='material-icons'>cancel</i></button></td></tr>"
	    	contadorPiezas++;
	    	$("#resultados-piezas-agregadas").slideToggle()
	    else
	    	alert "Complete todos los campos de pieza"

    #==========================================
    #oficios insupro checkbox
    #==========================================
    $("#color-insupro-piezas").hide()
			
			
